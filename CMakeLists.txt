CMAKE_MINIMUM_REQUIRED(VERSION 2.6)
PROJECT(qmi-plugin C)

### Global setting ###
SET(PREFIX ${CMAKE_INSTALL_PREFIX})
SET(EXEC_PREFIX "\${prefix}")
SET(LIBDIR ${LIB_INSTALL_DIR})
SET(INCLUDEDIR "\${prefix}/include")
SET(DATAROOTDIR "\${prefix}/share")

# Set required packages
INCLUDE(FindPkgConfig)
pkg_check_modules(pkgs REQUIRED glib-2.0 qmi-glib tcore dlog libtzplatform-config)

FOREACH(flag ${pkgs_CFLAGS})
	SET(EXTRA_CFLAGS "${EXTRA_CFLAGS} ${flag}")
ENDFOREACH(flag)

INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/include/)

SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${EXTRA_CFLAGS} -O2 -Werror -Wextra -Wno-unused-parameter -Wno-missing-field-initializers -Wdeclaration-after-statement -Wmissing-declarations -Wredundant-decls -Wcast-align -Wall -Wno-array-bounds -Wno-empty-body -Wno-ignored-qualifiers -Wshadow -Wwrite-strings -Wswitch-default -Wno-unused-but-set-parameter -Wno-unused-but-set-variable")

ADD_DEFINITIONS("-DFEATURE_TLOG_DEBUG")
ADD_DEFINITIONS("-DTCORE_LOG_TAG=\"QMI\"")

MESSAGE(${CMAKE_C_FLAGS})
#MESSAGE(${CMAKE_EXE_LINKER_FLAGS})
MESSAGE("FLAGS: ${CMAKE_EXE_LINKER_FLAGS}")

SET(SRCS
	src/desc_qmi.c
	src/s_call.c
	src/s_modem.c
	src/s_network.c
	src/s_ps.c
	src/s_sim.c
	src/s_sms.c
)



# library build
ADD_LIBRARY(qmi-plugin SHARED ${SRCS})
TARGET_LINK_LIBRARIES(qmi-plugin ${pkgs_LDFLAGS})
SET_TARGET_PROPERTIES(qmi-plugin PROPERTIES PREFIX "" OUTPUT_NAME qmi-plugin)


# install
INSTALL(TARGETS qmi-plugin
		LIBRARY DESTINATION ${LIBDIR}/telephony/plugins/modems)
