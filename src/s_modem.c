/*
 * tel-plugin-qmi
 *
 * Copyright (c) 2012 Samsung Electronics Co., Ltd. All rights reserved.
 * Copyright (c) 2021 Christof Meerwald. All rights reserved.
 *
 * Contact: Hayoon Ko <hayoon.ko@samsung.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>

#include <tzplatform_config.h>
#include <tcore.h>
#include <user_request.h>
#include <core_object.h>
#include <plugin.h>
#include <server.h>
#include <storage.h>
#include <co_modem.h>
#include <co_sim.h>

#include "s_common.h"
#include "s_modem.h"


static void set_operating_mode_ready(QmiClientDms *client,
    GAsyncResult *res, UserRequest *ur, gboolean on)
{
  QmiMessageDmsSetOperatingModeOutput *output;

  output = qmi_client_dms_set_operating_mode_finish(client, res, NULL);
  if (output)
  {
    gboolean result =  qmi_message_dms_set_operating_mode_output_get_result(output, NULL);

    if (on)
    {
      struct tresp_modem_power_on resp = { };
      resp.result = result ? TCORE_RETURN_SUCCESS : TCORE_RETURN_FAILURE;
      tcore_user_request_send_response(ur,
          TRESP_MODEM_POWER_ON,
          sizeof(resp), &resp);
    }
    else
    {
      struct tresp_modem_power_off resp = { };
      resp.result = result ? TCORE_RETURN_SUCCESS : TCORE_RETURN_FAILURE;
      tcore_user_request_send_response(ur,
          TRESP_MODEM_POWER_OFF,
          sizeof(resp), &resp);
    }

    qmi_message_dms_set_operating_mode_output_unref(output);
  }
}

static void set_operating_mode_online_ready(QmiClientDms *client,
    GAsyncResult *res, UserRequest *ur)
{
  set_operating_mode_ready(client, res, ur, TRUE);
}

static void set_operating_mode_offline_ready(QmiClientDms *client,
    GAsyncResult *res, UserRequest *ur)
{
  set_operating_mode_ready(client, res, ur, FALSE);
}

static TReturn power_on(CoreObject *co_modem, UserRequest *ur)
{
  PluginData *user_data =
      tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_modem));
  QmiMessageDmsSetOperatingModeInput *input = NULL;

  input = qmi_message_dms_set_operating_mode_input_new();
  qmi_message_dms_set_operating_mode_input_set_mode(input,
      QMI_DMS_OPERATING_MODE_ONLINE, NULL);

  qmi_client_dms_set_operating_mode(user_data->dms_client,
      input,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) set_operating_mode_online_ready,
      ur);

  qmi_message_dms_set_operating_mode_input_unref(input);

  return TCORE_RETURN_SUCCESS;
}

static TReturn power_off(CoreObject *co_modem, UserRequest *ur)
{
  PluginData *user_data =
      tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_modem));
  QmiMessageDmsSetOperatingModeInput *input = NULL;

  input = qmi_message_dms_set_operating_mode_input_new();
  qmi_message_dms_set_operating_mode_input_set_mode(input,
      QMI_DMS_OPERATING_MODE_LOW_POWER, NULL);

  qmi_client_dms_set_operating_mode(user_data->dms_client,
      input,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) set_operating_mode_offline_ready,
      ur);

  qmi_message_dms_set_operating_mode_input_unref(input);

  return TCORE_RETURN_SUCCESS;
}

static TReturn power_reset(CoreObject *co_modem, UserRequest *ur)
{
  dbg("Modem Power RESET request: NOT supported!!!");

  return TCORE_RETURN_ENOSYS;
}

static TReturn power_low(CoreObject *co_modem, UserRequest *ur)
{
  dbg("Modem Power LOW request: NOT supported!!!");

  return TCORE_RETURN_ENOSYS;
}


static void set_flight_mode_ready(QmiClientDms *client,
    GAsyncResult *res, UserRequest *ur)
{
  QmiMessageDmsSetOperatingModeOutput *output;

  struct tresp_modem_set_flightmode flight_resp = { 0 };

  output = qmi_client_dms_set_operating_mode_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_dms_set_operating_mode_output_get_result(output, NULL))
  {
    flight_resp.result = TCORE_RETURN_SUCCESS;
  }
  else
  {
    flight_resp.result = TCORE_RETURN_FAILURE;
  }

  tcore_user_request_send_response(ur,
      TRESP_MODEM_SET_FLIGHTMODE,
      sizeof(flight_resp), &flight_resp);

  qmi_message_dms_set_operating_mode_output_unref(output);
}

static void power_off_sim_ready(QmiClientUim *client,
    GAsyncResult *res, gpointer *p)
{
  QmiMessageUimPowerOffSimOutput *output =
      qmi_client_uim_power_off_sim_finish(client, res, NULL);
  qmi_message_uim_power_off_sim_output_unref(output);
}

static void power_on_sim_ready(QmiClientUim *client,
    GAsyncResult *res, gpointer *p)
{
  QmiMessageUimPowerOnSimOutput *output =
      qmi_client_uim_power_on_sim_finish(client, res, NULL);
  qmi_message_uim_power_on_sim_output_unref(output);
}

static TReturn set_flight_mode(CoreObject *co_modem, UserRequest *ur)
{
  PluginData *user_data =
      tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_modem));
  Server *s = tcore_plugin_ref_server(tcore_object_ref_plugin(co_modem));
  QmiMessageDmsSetOperatingModeInput *input = NULL;
  const struct treq_modem_set_flightmode *req_data;
  Storage *strg;
  gboolean flight_mode_state = FALSE;
  struct tnoti_modem_flight_mode flight_mode;

  req_data = tcore_user_request_ref_data(ur, NULL);

  dbg("req_data->enable: [%d]", req_data->enable);
  if (req_data->enable)
  {
    dbg("Flight mode - [ON]");
    flight_mode_state = TRUE;
  }
  else
  {
    dbg("Flight mode - [OFF]");
    flight_mode_state = FALSE;
  }

  strg = tcore_server_find_storage(s, "vconf");
  tcore_storage_set_bool(strg, STORAGE_KEY_FLIGHT_MODE_BOOL, flight_mode_state);

  input = qmi_message_dms_set_operating_mode_input_new();
  qmi_message_dms_set_operating_mode_input_set_mode(input,
      (req_data->enable
      ? QMI_DMS_OPERATING_MODE_LOW_POWER
      : QMI_DMS_OPERATING_MODE_ONLINE), NULL);

  qmi_client_dms_set_operating_mode(user_data->dms_client,
      input,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) set_flight_mode_ready,
      ur);

  qmi_message_dms_set_operating_mode_input_unref(input);

  if (user_data->uim_client != NULL)
  {
    if (flight_mode_state)
    {
      QmiMessageUimPowerOffSimInput *uim_input =
          qmi_message_uim_power_off_sim_input_new();
      qmi_message_uim_power_off_sim_input_set_slot(uim_input, 1, NULL);

      qmi_client_uim_power_off_sim(user_data->uim_client,
          uim_input,
          10,
          user_data->cancellable,
          (GAsyncReadyCallback) power_off_sim_ready,
          NULL);
    }
    else
    {
      QmiMessageUimPowerOnSimInput *uim_input =
          qmi_message_uim_power_on_sim_input_new();
      qmi_message_uim_power_on_sim_input_set_slot(uim_input, 1, NULL);

      qmi_client_uim_power_on_sim(user_data->uim_client,
          uim_input,
          10,
          user_data->cancellable,
          (GAsyncReadyCallback) power_on_sim_ready,
          NULL);
    }
  }

  /* Update Core Object */
  tcore_modem_set_flight_mode_state(co_modem, flight_mode_state);

  /* Send notification */
  flight_mode.enable = flight_mode_state;
  tcore_server_send_notification(s,
      co_modem, TNOTI_MODEM_FLIGHT_MODE,
      sizeof(flight_mode), &flight_mode);

  return TCORE_RETURN_SUCCESS;
}

static TReturn get_flight_mode(CoreObject *co_modem, UserRequest *ur)
{
  struct tresp_modem_get_flightmode resp_data = { };
  TReturn ret;

  resp_data.enable = tcore_modem_get_flight_mode_state(co_modem);
  resp_data.result = TCORE_RETURN_SUCCESS;
  dbg("Get Flight mode: Flight mdoe: [%s]", (resp_data.enable ? "ON" : "OFF"));

  ret = tcore_user_request_send_response(ur,
      TRESP_MODEM_GET_FLIGHTMODE,
      sizeof(resp_data), &resp_data);
  dbg("ret: [0x%x]", ret);

  return ret;
}

static void get_ids_ready_imei(QmiClientDms *client, GAsyncResult *res,
    UserRequest *ur)
{
  QmiMessageDmsGetIdsOutput *output;
  const gchar *imei = NULL;
  struct tresp_modem_get_imei imei_resp = { };

  output = qmi_client_dms_get_ids_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_dms_get_ids_output_get_result(output, NULL) &&
      qmi_message_dms_get_ids_output_get_imei(output, &imei, NULL))
  {
    g_strlcpy(imei_resp.imei, imei, 16+1);
    imei_resp.result = TCORE_RETURN_SUCCESS;
  }
  else
  {
    imei_resp.result = TCORE_RETURN_FAILURE;
  }

  tcore_user_request_send_response(ur,
      TRESP_MODEM_GET_IMEI,
      sizeof(struct tresp_modem_get_imei), &imei_resp);

  qmi_message_dms_get_ids_output_unref(output);
}

static TReturn get_imei(CoreObject *co_modem, UserRequest *ur)
{
  PluginData *user_data =
      tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_modem));

  qmi_client_dms_get_ids(user_data->dms_client,
      NULL,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) get_ids_ready_imei,
      ur);

  return TCORE_RETURN_SUCCESS;
}

static void get_ids_ready_sn(QmiClientDms *client, GAsyncResult *res,
UserRequest *ur)
{
  QmiMessageDmsGetIdsOutput *output;
  const gchar *str = NULL;
  struct tresp_modem_get_sn sn_resp = { };

  output = qmi_client_dms_get_ids_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_dms_get_ids_output_get_result(output, NULL))
  {
    if (qmi_message_dms_get_ids_output_get_esn(output, &str, NULL))
    {
      g_strlcpy(sn_resp.sn, str, MODEM_DEVICE_SN_LEN_MAX);
    }

    if (qmi_message_dms_get_ids_output_get_meid(output, &str, NULL))
    {
      g_strlcpy(sn_resp.meid, str, MODEM_DEVICE_MEID_LEN_MAX);
    }

    if (qmi_message_dms_get_ids_output_get_imei(output, &str, NULL))
    {
      g_strlcpy(sn_resp.imei, str, MODEM_DEVICE_IMEI_LEN_MAX);
    }

    if (qmi_message_dms_get_ids_output_get_imei_software_version(output,
            &str, NULL))
    {
      g_strlcpy(sn_resp.imeisv, str, MODEM_DEVICE_IMEISV_LEN_MAX);
    }

    sn_resp.result = TCORE_RETURN_SUCCESS;
  }
  else
  {
    sn_resp.result = TCORE_RETURN_FAILURE;
  }

  tcore_user_request_send_response(ur,
      TRESP_MODEM_GET_SN,
      sizeof(struct tresp_modem_get_sn), &sn_resp);

  qmi_message_dms_get_ids_output_unref(output);
}

static TReturn get_sn(CoreObject *co_modem, UserRequest *ur)
{
  PluginData *user_data;

  user_data = tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_modem));
  qmi_client_dms_get_ids(user_data->dms_client,
      NULL,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) get_ids_ready_sn,
      ur);

  return TCORE_RETURN_SUCCESS;
}


static enum modem_state get_modem_state(QmiDmsOperatingMode operating_mode)
{
  enum modem_state status;

  switch (operating_mode)
  {
  case QMI_DMS_OPERATING_MODE_ONLINE:
    status = MODEM_STATE_ONLINE;
    break;

  case QMI_DMS_OPERATING_MODE_LOW_POWER:
  case QMI_DMS_OPERATING_MODE_PERSISTENT_LOW_POWER:
  case QMI_DMS_OPERATING_MODE_MODE_ONLY_LOW_POWER:
    status = MODEM_STATE_LOW;
    break;

  case QMI_DMS_OPERATING_MODE_OFFLINE:
  case QMI_DMS_OPERATING_MODE_SHUTTING_DOWN:
    status = MODEM_STATE_OFFLINE;
    break;

  case QMI_DMS_OPERATING_MODE_RESET:
  case QMI_DMS_OPERATING_MODE_FACTORY_TEST:
  default:
    status = MODEM_STATE_UNKNOWN;
  }

  return status;
}

static void event_report(QmiClientLoc *client,
    QmiIndicationDmsEventReportOutput *output, PluginData *user_data)
{
  CoreObject *co_modem =
      tcore_plugin_ref_core_object(user_data->plugin, CORE_OBJECT_TYPE_MODEM);
  Server *s = tcore_plugin_ref_server(user_data->plugin);

  QmiDmsOperatingMode operating_mode;

  if (qmi_indication_dms_event_report_output_get_operating_mode(output,
          &operating_mode, NULL))
  {
    struct tnoti_modem_power modem_power = { };

    modem_power.state = get_modem_state(operating_mode);

    if (modem_power.state == MODEM_STATE_ONLINE)
    {
      tcore_modem_set_powered(co_modem, TRUE);
    }
    else if ((modem_power.state == MODEM_STATE_OFFLINE) ||
        (modem_power.state == MODEM_STATE_LOW))
    {
      tcore_modem_set_powered(co_modem, FALSE);
    }

    tcore_server_send_notification(s,
        co_modem, TNOTI_MODEM_POWER,
        sizeof(modem_power), &modem_power);
  }
}

static void set_operating_mode_init_ready(QmiClientDms *client,
    GAsyncResult *res, gpointer data)
{ }

static void get_operating_mode_ready(QmiClientDms *client,
    GAsyncResult *res, gpointer data)
{
  QmiMessageDmsGetOperatingModeOutput *output;

  output = qmi_client_dms_get_operating_mode_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_dms_get_operating_mode_output_get_result(output, NULL))
  {
    PluginData *user_data = (PluginData *) data;
    Server *s = tcore_plugin_ref_server(user_data->plugin);
    CoreObject *co_modem = tcore_plugin_ref_core_object(user_data->plugin, CORE_OBJECT_TYPE_MODEM);

    QmiDmsOperatingMode operating_mode;
    gboolean flight_mode;
    struct tnoti_modem_power modem_power = { };

    qmi_message_dms_get_operating_mode_output_get_mode(output, &operating_mode, NULL);

    modem_power.state = get_modem_state(operating_mode);

    if (modem_power.state == MODEM_STATE_ONLINE)
    {
      tcore_modem_set_powered(co_modem, TRUE);
    }
    else
    {
      tcore_modem_set_powered(co_modem, FALSE);
    }

    tcore_server_send_notification(s,
        co_modem, TNOTI_MODEM_POWER,
        sizeof(modem_power), &modem_power);

    flight_mode = tcore_modem_get_flight_mode_state(co_modem);
    if (flight_mode != (modem_power.state != MODEM_STATE_ONLINE))
    {
      QmiMessageDmsSetOperatingModeInput *input = NULL;

      input = qmi_message_dms_set_operating_mode_input_new();
      qmi_message_dms_set_operating_mode_input_set_mode(input,
          (flight_mode
              ? QMI_DMS_OPERATING_MODE_LOW_POWER
              : QMI_DMS_OPERATING_MODE_ONLINE), NULL);

      qmi_client_dms_set_operating_mode(user_data->dms_client,
          input,
          10,
          user_data->cancellable,
          (GAsyncReadyCallback) set_operating_mode_init_ready,
          data);

      qmi_message_dms_set_operating_mode_input_unref(input);
    }
  }
}

static gboolean init_done(gpointer data)
{
  PluginData *user_data = data;
  CoreObject *co_modem =
      tcore_plugin_ref_core_object(user_data->plugin, CORE_OBJECT_TYPE_MODEM);
  Server *s = tcore_plugin_ref_server(user_data->plugin);
  Storage *strg = tcore_server_find_storage(s, "vconf");

  gboolean flight_mode;

  flight_mode = tcore_storage_get_bool(strg, STORAGE_KEY_FLIGHT_MODE_BOOL);
  tcore_modem_set_flight_mode_state(co_modem, flight_mode);

  qmi_client_dms_get_operating_mode(user_data->dms_client,
      NULL,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) get_operating_mode_ready,
      user_data);

  return FALSE;
}

static void set_event_report_ready(QmiClientDms *client, GAsyncResult *res,
    PluginData *user_data)
{
  QmiMessageDmsSetEventReportOutput *output;

  output = qmi_client_dms_set_event_report_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_dms_set_event_report_output_get_result(output, NULL))
  {
    user_data->dms_event_report_id = g_signal_connect(client, "event-report",
        G_CALLBACK(event_report), user_data);
  }

  qmi_message_dms_set_event_report_output_unref(output);
}

static void allocate_client_ready(QmiDevice *dev, GAsyncResult *res,
    PluginData *user_data)
{
  QmiClient *client = qmi_device_allocate_client_finish(dev, res, NULL);

  user_data->dms_client = QMI_CLIENT_DMS(client);

  {
    QmiMessageDmsSetEventReportInput *input =
        qmi_message_dms_set_event_report_input_new();

    qmi_message_dms_set_event_report_input_set_operating_mode_reporting(input,
        TRUE, NULL);
    qmi_client_dms_set_event_report(user_data->dms_client,
        input,
        10,
        user_data->cancellable,
        (GAsyncReadyCallback) set_event_report_ready,
        user_data);
    qmi_message_dms_set_event_report_input_unref(input);
  }

  user_data->modem_inited = TRUE;

  g_timeout_add_full(G_PRIORITY_HIGH, 100, init_done, user_data, 0);
}

static void release_client_ready(QmiDevice *dev, GAsyncResult *res,
    PluginData *user_data)
{
  qmi_device_release_client_finish(dev, res, NULL);

  user_data->dms_client = NULL;
}


/** Modem operations */
static struct tcore_modem_operations modem_ops = {
  .power_on = power_on,
  .power_off = power_off,
  .power_reset = power_reset,
  .power_low = power_low,
  .set_flight_mode = set_flight_mode,
  .get_imei = get_imei,
  .get_version = NULL,
  .get_sn = get_sn,
  .get_flight_mode = get_flight_mode,
};

gboolean s_modem_init(TcorePlugin *p, PluginData *user_data)
{
  CoreObject *co_modem;

  co_modem = tcore_modem_new(p, "modem", &modem_ops, NULL);
  if (!co_modem)
  {
    err("Core object is NULL");
    return FALSE;
  }

  qmi_device_allocate_client(user_data->device,
      QMI_SERVICE_DMS,
      QMI_CID_NONE,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) allocate_client_ready,
      user_data);

  return TRUE;
}

void s_modem_exit(TcorePlugin *p)
{
  PluginData *user_data = tcore_plugin_ref_user_data(p);
  CoreObject *co_modem = tcore_plugin_ref_core_object(p, CORE_OBJECT_TYPE_MODEM);

  if (user_data->dms_event_report_id)
  {
    g_signal_handler_disconnect(user_data->dms_client,
        user_data->dms_event_report_id);
  }

  qmi_device_release_client(user_data->device,
      QMI_CLIENT(user_data->dms_client),
      QMI_DEVICE_RELEASE_CLIENT_FLAGS_NONE,
      10,
      NULL,
      (GAsyncReadyCallback) release_client_ready,
      NULL);

  tcore_modem_free(co_modem);
}
