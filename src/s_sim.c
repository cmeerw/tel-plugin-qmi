/*
 * tel-plugin-qmi
 *
 * Copyright (c) 2012 Samsung Electronics Co., Ltd. All rights reserved.
 * Copyright (c) 2021 Christof Meerwald. All rights reserved.
 *
 * Contact: Hayoon Ko <hayoon.ko@samsung.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>

#include <tzplatform_config.h>
#include <tcore.h>
#include <core_object.h>
#include <user_request.h>
#include <plugin.h>
#include <server.h>
#include <co_sim.h>

#include "s_common.h"
#include "s_sim.h"


static void set_poweroff_ready(QmiClientUim *client, GAsyncResult *res,
    UserRequest *ur)
{
  QmiMessageUimPowerOffSimOutput *output;

  struct tresp_sim_set_powerstate resp = { };

  output = qmi_client_uim_power_off_sim_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_uim_power_off_sim_output_get_result(output, NULL))
  {
    resp.result = SIM_POWER_SET_SUCCESS;
  }
  else
  {
    resp.result = SIM_POWER_SET_FAILURE;
  }

  tcore_user_request_send_response(ur,
      TRESP_SIM_SET_POWERSTATE,
      sizeof(resp), &resp);

  qmi_message_uim_power_off_sim_output_unref(output);
}

static void set_poweron_ready(QmiClientUim *client, GAsyncResult *res,
    UserRequest *ur)
{
  QmiMessageUimPowerOnSimOutput *output;

  struct tresp_sim_set_powerstate resp = { };

  output = qmi_client_uim_power_on_sim_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_uim_power_on_sim_output_get_result(output, NULL))
  {
    resp.result = SIM_POWER_SET_SUCCESS;
  }
  else
  {
    resp.result = SIM_POWER_SET_FAILURE;
  }

  tcore_user_request_send_response(ur,
      TRESP_SIM_SET_POWERSTATE,
      sizeof(resp), &resp);

  qmi_message_uim_power_on_sim_output_unref(output);
}

static TReturn set_powerstate(CoreObject *co_sim, UserRequest *ur)
{
  PluginData *user_data =
      tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_sim));

  struct treq_sim_set_powerstate *powerstate_req =
      (struct treq_sim_set_powerstate *) tcore_user_request_ref_data(ur, NULL);

  switch (powerstate_req->state)
  {
  case SIM_POWER_OFF:
    {
      QmiMessageUimPowerOffSimInput *input = qmi_message_uim_power_off_sim_input_new();
      qmi_message_uim_power_off_sim_input_set_slot(input, 1, NULL);

      qmi_client_uim_power_off_sim(user_data->uim_client,
          input,
          10,
          user_data->cancellable,
          (GAsyncReadyCallback) set_poweroff_ready,
          ur);

      qmi_message_uim_power_off_sim_input_unref(input);
    }
    break;

  case SIM_POWER_ON:
    {
      QmiMessageUimPowerOnSimInput *input = qmi_message_uim_power_on_sim_input_new();
      qmi_message_uim_power_on_sim_input_set_slot(input, 1, NULL);

      qmi_client_uim_power_on_sim(user_data->uim_client,
          input,
          10,
          user_data->cancellable,
          (GAsyncReadyCallback) set_poweron_ready,
          ur);

      qmi_message_uim_power_on_sim_input_unref(input);
    }
    break;

  case SIM_POWER_UNSPECIFIED:
  default:
    return TCORE_RETURN_FAILURE;
  }

  return TCORE_RETURN_SUCCESS;
}


static void verify_pins_ready(QmiClientUim *client, GAsyncResult *res,
    UserRequest *ur)
{
  QmiMessageUimVerifyPinOutput *output;

  struct tresp_sim_verify_pins resp = { };

  output = qmi_client_uim_verify_pin_finish(client, res, NULL);
  if (output != NULL)
  {
    guint8 verify_retries_left;
    guint8 unblock_retries_left;

    if (!qmi_message_uim_verify_pin_output_get_result(output, NULL))
    {
      resp.result = SIM_INCORRECT_PASSWORD;
    }
    else
    {
      resp.result = SIM_PIN_OPERATION_SUCCESS;
    }

    if (qmi_message_uim_verify_pin_output_get_retries_remaining(output,
            &verify_retries_left, &unblock_retries_left, NULL))
    {
      resp.retry_count = verify_retries_left;
    }
  }

  tcore_user_request_send_response(ur,
      TRESP_SIM_VERIFY_PINS,
      sizeof(resp), &resp);

  qmi_message_uim_verify_pin_output_unref(output);
}

static TReturn verify_pins(CoreObject *co_sim, UserRequest *ur)
{
  PluginData *user_data =
      tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_sim));

  struct treq_sim_verify_pins *verify_pins_req =
      (struct treq_sim_verify_pins *) tcore_user_request_ref_data(ur, NULL);
  QmiMessageUimVerifyPinInput *input = NULL;
  QmiUimPinId pin_id;
  GArray *dummy_aid;

  switch (verify_pins_req->pin_type)
  {
  case SIM_PTYPE_PIN1:
    pin_id = QMI_UIM_PIN_ID_PIN1;
    break;

  case SIM_PTYPE_PIN2:
    pin_id = QMI_UIM_PIN_ID_PIN2;
    break;

  case SIM_PTYPE_UPIN:
    pin_id = QMI_UIM_PIN_ID_UPIN;
    break;

  default:
    pin_id = QMI_UIM_PIN_ID_UNKNOWN;
    break;
  }

  input = qmi_message_uim_verify_pin_input_new();
  qmi_message_uim_verify_pin_input_set_info(input,
      pin_id, verify_pins_req->pin, NULL);

  dummy_aid = g_array_new(FALSE, FALSE, sizeof (guint8));
  qmi_message_uim_verify_pin_input_set_session(input,
      QMI_UIM_SESSION_TYPE_CARD_SLOT_1, dummy_aid /* ignored */, NULL);
  g_array_unref(dummy_aid);

  qmi_client_uim_verify_pin(user_data->uim_client,
      input,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) verify_pins_ready,
      ur);

  qmi_message_uim_verify_pin_input_unref(input);

  return TCORE_RETURN_SUCCESS;
}


static void get_msisdn_ready(QmiClientDms *client, GAsyncResult *res,
    UserRequest *ur)
{
  QmiMessageDmsGetMsisdnOutput *output;

  struct tresp_sim_read files = { };
  const gchar *str = NULL;

  output = qmi_client_dms_get_msisdn_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_dms_get_msisdn_output_get_result(output, NULL) &&
      qmi_message_dms_get_msisdn_output_get_msisdn(output, &str, NULL))
  {
    files.data.msisdn_list.count = 1;
    strcpy((char *) files.data.msisdn_list.msisdn[0].num, str);
    files.data.msisdn_list.msisdn[0].ton = SIM_TON_INTERNATIONAL;
    strcpy((char *) files.data.msisdn_list.msisdn[0].name, "");
    files.data.msisdn_list.msisdn[0].next_record = 0;
    files.result = SIM_ACCESS_SUCCESS;
  }
  else
  {
    files.result = SIM_ACCESS_FAILED;
  }

  tcore_user_request_send_response(
    ur, TRESP_SIM_GET_MSISDN, sizeof(files), &files);

  qmi_message_dms_get_msisdn_output_unref(output);
}

static TReturn sim_read_file(CoreObject *co_sim, UserRequest *ur)
{
  TReturn ret = TCORE_RETURN_SUCCESS;
  PluginData *user_data = tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_sim));
  enum tcore_request_command command = tcore_user_request_get_command(ur);

  switch (command) {
  case TREQ_SIM_GET_MSISDN:
    qmi_client_dms_get_msisdn(user_data->dms_client,
        NULL,
        10,
        user_data->cancellable,
        (GAsyncReadyCallback) get_msisdn_ready,
        ur);
    break;

  case TREQ_SIM_GET_ECC:
  {
    struct tresp_sim_read ecc = { };
    ecc.result = TCORE_RETURN_SUCCESS;
    ecc.data.ecc.ecc_count = 0;

    tcore_user_request_send_response(ur,
        TRESP_SIM_GET_ECC,
        sizeof(ecc), &ecc);
  }
  break;

  case TREQ_SIM_GET_LANGUAGE:
  case TREQ_SIM_GET_CALLFORWARDING:
  case TREQ_SIM_GET_SPN:
  case TREQ_SIM_GET_SPDI:
  case TREQ_SIM_GET_SERVICE_TABLE:
  case TREQ_SIM_GET_ICCID:
  case TREQ_SIM_GET_GID:
  case TREQ_SIM_GET_MESSAGEWAITING:
  case TREQ_SIM_GET_CPHS_INFO:
  case TREQ_SIM_GET_OPL:
  case TREQ_SIM_GET_PNN:
  case TREQ_SIM_GET_CPHS_NETNAME:
  case TREQ_SIM_GET_OPLMNWACT:
  case TREQ_SIM_GET_MAILBOX:
  default:
    dbg("Unhandled read requests - command: [0x%x]", command);
    ret = TCORE_RETURN_EINVAL;
    break;
  }
  dbg("ret: [0x%x]", ret);

  return ret;
}


static enum tel_sim_app_type get_sim_apps(QmiMessageUimGetCardStatusOutputCardStatusCardsElement *card)
{
  enum tel_sim_app_type sim_apps = SIM_APP_TYPE_UNKNOWN;
  guint i;

  for (i = 0; i < card->applications->len; ++i)
  {
    QmiMessageUimGetCardStatusOutputCardStatusCardsElementApplicationsElement *app =
        &g_array_index(card->applications, QmiMessageUimGetCardStatusOutputCardStatusCardsElementApplicationsElement, i);

    switch (app->type)
    {
    case QMI_UIM_CARD_APPLICATION_TYPE_SIM:
      sim_apps = sim_apps | SIM_APP_TYPE_SIM;
      break;

    case QMI_UIM_CARD_APPLICATION_TYPE_USIM:
      sim_apps = sim_apps | SIM_APP_TYPE_USIM;
      break;

    case QMI_UIM_CARD_APPLICATION_TYPE_RUIM:
      sim_apps = sim_apps | SIM_APP_TYPE_RUIM;
      break;

    case QMI_UIM_CARD_APPLICATION_TYPE_CSIM:
      sim_apps = sim_apps | SIM_APP_TYPE_CSIM;
      break;

    case QMI_UIM_CARD_APPLICATION_TYPE_ISIM:
      sim_apps = sim_apps | SIM_APP_TYPE_ISIM;
      break;

    case QMI_UIM_CARD_APPLICATION_TYPE_UNKNOWN:
    default:
      break;
    }
  }

  return sim_apps;
}

static enum tel_sim_status get_sim_status(QmiMessageUimGetCardStatusOutputCardStatusCardsElement *card)
{
  enum tel_sim_status sim_status;

  switch (card->card_state)
  {
  case QMI_UIM_CARD_STATE_PRESENT:
    {
      QmiMessageUimGetCardStatusOutputCardStatusCardsElementApplicationsElement *app;
      app = &g_array_index(card->applications, QmiMessageUimGetCardStatusOutputCardStatusCardsElementApplicationsElement, 0);
      switch (app->pin1_state)
      {
      case QMI_UIM_PIN_STATE_NOT_INITIALIZED:
        sim_status = SIM_STATUS_CARD_INSERTED;
        break;

      case QMI_UIM_PIN_STATE_ENABLED_NOT_VERIFIED:
        sim_status = SIM_STATUS_PIN_REQUIRED;
        break;

      case QMI_UIM_PIN_STATE_ENABLED_VERIFIED:
        sim_status = SIM_STATUS_INIT_COMPLETED;
        break;

      case QMI_UIM_PIN_STATE_DISABLED:
        sim_status = SIM_STATUS_INIT_COMPLETED;
        break;

      case QMI_UIM_PIN_STATE_BLOCKED:
      case QMI_UIM_PIN_STATE_PERMANENTLY_BLOCKED:
        sim_status = SIM_STATUS_CARD_BLOCKED;
        break;

      default:
        sim_status = SIM_STATUS_INITIALIZING;
        break;
      }
    }
    break;

  case QMI_UIM_CARD_STATE_ABSENT:
    sim_status = SIM_STATUS_CARD_NOT_PRESENT;
    break;

  default:
    sim_status = SIM_STATUS_CARD_ERROR;
    break;
  }

  return sim_status;
}

static void uim_get_imsi_ready(QmiClientDms *client, GAsyncResult *res,
    PluginData *user_data)
{
  QmiMessageDmsUimGetImsiOutput *output;

  const gchar *imsi;

  output = qmi_client_dms_uim_get_imsi_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_dms_uim_get_imsi_output_get_result(output, NULL) &&
      qmi_message_dms_uim_get_imsi_output_get_imsi(output, &imsi, NULL))
  {
    Server *s = tcore_plugin_ref_server(user_data->plugin);
    CoreObject *co_sim = tcore_plugin_ref_core_object(user_data->plugin, CORE_OBJECT_TYPE_SIM);

    enum tel_sim_status sim_status = SIM_STATUS_INIT_COMPLETED;
    struct tnoti_sim_status noti = { };
    struct tel_sim_imsi tel_imsi = { };

    memcpy(tel_imsi.plmn, imsi, 5);
    tel_imsi.plmn[5] = '\0';
    strcpy(tel_imsi.msin, imsi + 5);

    noti.sim_status = sim_status;
    noti.b_changed = FALSE;

    tcore_sim_set_status(co_sim, sim_status);
    tcore_sim_set_imsi(co_sim, &tel_imsi);

    tcore_server_send_notification(s,
        co_sim, TNOTI_SIM_STATUS,
        sizeof(noti), &noti);
  }

  qmi_message_dms_uim_get_imsi_output_unref(output);
}

static void get_card_status_ready(QmiClientUim *client, GAsyncResult *res,
    PluginData *user_data)
{
  QmiMessageUimGetCardStatusOutput *output;

  guint16 index_gw_primary;
  guint16 index_1x_primary;
  guint16 index_gw_secondary;
  guint16 index_1x_secondary;
  GArray *cards;

  output = qmi_client_uim_get_card_status_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_uim_get_card_status_output_get_result(output, NULL) &&
      qmi_message_uim_get_card_status_output_get_card_status(
        output,
        &index_gw_primary, &index_1x_primary,
        &index_gw_secondary, &index_1x_secondary,
        &cards, NULL))
  {
    if (cards->len != 0)
    {
      CoreObject *co_sim =
          tcore_plugin_ref_core_object(user_data->plugin, CORE_OBJECT_TYPE_SIM);
      QmiMessageUimGetCardStatusOutputCardStatusCardsElement *card =
          &g_array_index(
            cards, QmiMessageUimGetCardStatusOutputCardStatusCardsElement, 0);

      enum tel_sim_app_type sim_apps = get_sim_apps(card);
      enum tel_sim_status sim_status = get_sim_status(card);

      tcore_sim_set_app_list(co_sim, sim_apps);

      if (sim_status == SIM_STATUS_INIT_COMPLETED)
      {
        qmi_client_dms_uim_get_imsi(user_data->dms_client,
            NULL,
            10,
            user_data->cancellable,
            (GAsyncReadyCallback) uim_get_imsi_ready,
            user_data);
      }
      else
      {
        Server *s = tcore_plugin_ref_server(user_data->plugin);

        struct tnoti_sim_status noti = { };
        struct tel_sim_imsi tel_imsi = { };

        noti.sim_status = sim_status;
        noti.b_changed = TRUE;

        tcore_sim_set_status(co_sim, sim_status);
        tcore_sim_set_imsi(co_sim, &tel_imsi);

        tcore_server_send_notification(s,
            co_sim, TNOTI_SIM_STATUS,
            sizeof(noti), &noti);
      }
    }
  }

  qmi_message_uim_get_card_status_output_unref(output);
}

static void card_status(QmiClientLoc *client,
    QmiIndicationUimCardStatusOutput *output, PluginData *user_data)
{
  guint16 index_gw_primary;
  guint16 index_1x_primary;
  guint16 index_gw_secondary;
  guint16 index_1x_secondary;
  GArray *cards;

  if (qmi_indication_uim_card_status_output_get_card_status(
        output,
        &index_gw_primary, &index_1x_primary,
        &index_gw_secondary, &index_1x_secondary,
        &cards, NULL))
  {
    if (cards->len != 0)
    {
      CoreObject *co_sim =
          tcore_plugin_ref_core_object(user_data->plugin, CORE_OBJECT_TYPE_SIM);
      QmiMessageUimGetCardStatusOutputCardStatusCardsElement *card =
          &g_array_index(
            cards, QmiMessageUimGetCardStatusOutputCardStatusCardsElement, 0);

      enum tel_sim_app_type sim_apps = get_sim_apps(card);
      enum tel_sim_status sim_status = get_sim_status(card);

      tcore_sim_set_app_list(co_sim, sim_apps);

      if (sim_status == SIM_STATUS_INIT_COMPLETED)
      {
        qmi_client_dms_uim_get_imsi(user_data->dms_client,
            NULL,
            10,
            user_data->cancellable,
            (GAsyncReadyCallback) uim_get_imsi_ready,
            user_data);
      }
      else
      {
        Server *s = tcore_plugin_ref_server(user_data->plugin);

        struct tnoti_sim_status noti = { };
        struct tel_sim_imsi tel_imsi = { };

        noti.sim_status = sim_status;
        noti.b_changed = TRUE;

        tcore_sim_set_status(co_sim, sim_status);
        tcore_sim_set_imsi(co_sim, &tel_imsi);

        tcore_server_send_notification(s,
            co_sim, TNOTI_SIM_STATUS,
            sizeof(noti), &noti);
      }
    }
  }
}

static void register_card_status_events_ready(QmiClientUim *client,
    GAsyncResult *res, PluginData *user_data)
{
  QmiMessageUimRegisterEventsOutput *output;

  output = qmi_client_uim_register_events_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_uim_register_events_output_get_result(output, NULL))
  {
    user_data->uim_slot_status_id = g_signal_connect(client, "card-status",
        G_CALLBACK(card_status), user_data);
  }

  qmi_message_uim_register_events_output_unref(output);
}

static gboolean init_done(gpointer data)
{
  PluginData *user_data = data;

  qmi_client_uim_get_card_status(user_data->uim_client,
      NULL,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) get_card_status_ready,
      user_data);

  return FALSE;
}

static void allocate_client_ready(QmiDevice *dev, GAsyncResult *res,
    PluginData *user_data)
{
  GError *error = NULL;
  QmiClient *client = qmi_device_allocate_client_finish(dev, res, &error);

  user_data->uim_client = QMI_CLIENT_UIM(client);

  {
    QmiMessageUimRegisterEventsInput *re_input =
        qmi_message_uim_register_events_input_new();

    qmi_message_uim_register_events_input_set_event_registration_mask(
      re_input, QMI_UIM_EVENT_REGISTRATION_FLAG_CARD_STATUS, NULL);
    qmi_client_uim_register_events(
      user_data->uim_client,
      re_input,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) register_card_status_events_ready,
      user_data);
    qmi_message_uim_register_events_input_unref(re_input);
  }

  user_data->sim_inited = TRUE;

  g_timeout_add_full(G_PRIORITY_HIGH, 100, init_done, user_data, 0);
}

static void release_client_ready(QmiDevice *dev, GAsyncResult *res,
    PluginData *user_data)
{
  qmi_device_release_client_finish(dev, res, NULL);
  user_data->uim_client = NULL;
}


/* SIM Operations */
static struct tcore_sim_operations sim_ops = {
  .verify_pins = verify_pins,
  .verify_puks = NULL,
  .change_pins = NULL,
  .get_facility_status = NULL,
  .enable_facility = NULL,
  .disable_facility = NULL,
  .get_lock_info = NULL,
  .read_file = sim_read_file,
  .update_file = NULL,
  .transmit_apdu = NULL,
  .get_atr = NULL,
  .req_authentication = NULL,
  .set_powerstate = set_powerstate
};


gboolean s_sim_init(TcorePlugin *p, PluginData *user_data)
{
  CoreObject *co_sim;

  co_sim = tcore_sim_new(p, "sim", &sim_ops, NULL);
  if (!co_sim)
  {
    err("Core object is NULL");
    return FALSE;
  }

  qmi_device_allocate_client(user_data->device,
      QMI_SERVICE_UIM,
      QMI_CID_NONE,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) allocate_client_ready,
      user_data);

  return TRUE;
}

void s_sim_exit(TcorePlugin *p)
{
  PluginData *user_data = tcore_plugin_ref_user_data(p);
  CoreObject *co_sim = tcore_plugin_ref_core_object(p, CORE_OBJECT_TYPE_SIM);

  if (user_data->uim_slot_status_id)
  {
    g_signal_handler_disconnect(user_data->uim_client,
        user_data->uim_slot_status_id);
  }

  qmi_device_release_client(user_data->device,
      QMI_CLIENT(user_data->uim_client),
      QMI_DEVICE_RELEASE_CLIENT_FLAGS_NONE,
      10,
      NULL,
      (GAsyncReadyCallback) release_client_ready,
      NULL);

  tcore_sim_free(co_sim);
}
