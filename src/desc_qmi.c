/*
 * tel-plugin-qmi
 *
 * Copyright (c) 2012 Samsung Electronics Co., Ltd. All rights reserved.
 * Copyright (c) 2021 Christof Meerwald. All rights reserved.
 *
 * Contact: Hayoon Ko <hayoon.ko@samsung.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>

#include <glib.h>
#include <libqmi-glib.h>

#include <tcore.h>
#include <plugin.h>
#include <hal.h>
#include <server.h>

#include "s_common.h"
#include "s_call.h"
#include "s_modem.h"
#include "s_network.h"
#include "s_ps.h"
#include "s_sim.h"
#include "s_sms.h"


static gboolean on_load()
{
  return TRUE;
}

static void device_open_ready(QmiDevice *dev, GAsyncResult *res, gpointer data)
{
  PluginData *user_data = (PluginData *) data;
  GError *error = NULL;

  if (!qmi_device_open_finish(dev, res, &error))
  {
    g_error("error: couldn't open the QmiDevice: %s\n", error->message);
  }

  user_data->device_opened = TRUE;
}

static void device_new_ready(GObject *unused, GAsyncResult *res, gpointer data)
{
  PluginData *user_data = (PluginData *) data;
  QmiDeviceOpenFlags open_flags = QMI_DEVICE_OPEN_FLAGS_PROXY | QMI_DEVICE_OPEN_FLAGS_EXPECT_INDICATIONS;
  GError *error = NULL;

  user_data->device = qmi_device_new_finish(res, &error);
  if (!user_data->device)
  {
    g_error("error: couldn't create QmiDevice: %s\n", error->message);
  }

  qmi_device_open(user_data->device,
      open_flags,
      15,
      user_data->cancellable,
      (GAsyncReadyCallback) device_open_ready,
      user_data);
}

static gboolean on_init(TcorePlugin *p)
{
  PluginData *user_data;

  if (!p)
  {
    err("Plug-in is NULL");
    return FALSE;
  }

  user_data = g_try_new0(PluginData, 1);
  if (user_data == NULL)
  {
    err(" Failed to allocate memory for Plugin data");
    return FALSE;
  }

  user_data->plugin = p;
  user_data->cancellable = g_cancellable_new();
  user_data->file = g_file_new_for_commandline_arg("/dev/modem");

  qmi_device_new(user_data->file, user_data->cancellable,
      (GAsyncReadyCallback) device_new_ready, user_data);

  while (!user_data->device_opened)
  {
    g_main_context_iteration(NULL, TRUE);
  }


  tcore_plugin_link_user_data(p, user_data);

  /* Initialize Modules */
  s_call_init(p, user_data);
  s_modem_init(p, user_data);
  s_network_init(p, user_data);
  s_ps_init(p, user_data);
  s_sim_init(p, user_data);
  s_sms_init(p, user_data);

  while (!user_data->call_inited || !user_data->modem_inited ||
      !user_data->network_inited || !user_data->ps_inited ||
      !user_data->sim_inited || !user_data->sms_inited)
  {
    g_main_context_iteration(NULL, TRUE);
  }


  dbg("Init - Successful");

  return TRUE;
}

static void on_unload(TcorePlugin *p)
{
  PluginData *user_data;

  if (!p)
  {
    err("Plug-in is NULL");
    return;
  }

  user_data = tcore_plugin_ref_user_data(p);
  if (user_data == NULL)
  {
    return;
  }

  /* De-initialize Modules */
  s_sms_exit(p);
  s_sim_exit(p);
  s_ps_exit(p);
  s_network_exit(p);
  s_modem_exit(p);
  s_call_exit(p);

  while (user_data->dms_client || user_data->nas_client ||
      user_data->uim_client || user_data->voice_client ||
      user_data->wda_client || user_data->wds_client ||
      user_data->wms_client)
  {
    g_main_context_iteration(NULL, TRUE);
  }

  g_object_unref(user_data->file);
  g_clear_object(&user_data->cancellable);

  g_free(user_data);
}

/* QMI plug-in descriptor */
struct tcore_plugin_define_desc plugin_define_desc = {
  .name = "QMI",
  .priority = TCORE_PLUGIN_PRIORITY_MID,
  .version = 1,
  .load = on_load,
  .init = on_init,
  .unload = on_unload
};
