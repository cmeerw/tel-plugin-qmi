/*
 * tel-plugin-qmi
 *
 * Copyright (c) 2012 Samsung Electronics Co., Ltd. All rights reserved.
 * Copyright (c) 2021 Christof Meerwald. All rights reserved.
 *
 * Contact: Hayoon Ko <hayoon.ko@samsung.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>

#include <tzplatform_config.h>
#include <tcore.h>
#include <core_object.h>
#include <user_request.h>
#include <plugin.h>
#include <server.h>
#include <storage.h>
#include <co_network.h>

#include "s_common.h"
#include "s_network.h"


static void search_network_ready(QmiClientNas *client,
    GAsyncResult *res, UserRequest *ur)
{
  QmiMessageNasNetworkScanOutput *output;
  struct tresp_network_search resp = { };

  output = qmi_client_nas_network_scan_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_nas_network_scan_output_get_result(output, NULL))
  {
    GArray *radio_access_technology;
    GArray *network_information;
    int i;

    resp.result = TCORE_RETURN_SUCCESS;

    qmi_message_nas_network_scan_output_get_network_information(output,
        &network_information, NULL);
    qmi_message_nas_network_scan_output_get_radio_access_technology(output,
        &radio_access_technology, NULL);

    resp.list_count = network_information->len;

    for (i = 0; i < resp.list_count; ++i)
    {
      QmiMessageNasNetworkScanOutputNetworkInformationElement *network_information_element;
      QmiMessageNasNetworkScanOutputRadioAccessTechnologyElement *radio_access_technology_element;

      network_information_element = &g_array_index(
        network_information,
        QmiMessageNasNetworkScanOutputNetworkInformationElement, i);
      radio_access_technology_element = &g_array_index(
        radio_access_technology,
        QmiMessageNasNetworkScanOutputRadioAccessTechnologyElement, i);

      if (network_information_element->network_status & QMI_NAS_NETWORK_STATUS_FORBIDDEN)
      {
        resp.list[i].status = NETWORK_PLMN_FORBIDDEN;
      }
      else if (network_information_element->network_status & QMI_NAS_NETWORK_STATUS_CURRENT_SERVING)
      {
        resp.list[i].status = NETWORK_PLMN_CURRENT;
      }
      else
      {
        resp.list[i].status = NETWORK_PLMN_AVAILABLE;
      }

      sprintf(resp.list[i].plmn, "%03u%02u",
          network_information_element->mcc,
          network_information_element->mnc);
      // TODO: strcpy(resp.list[i].name, network_information_element->description);

      switch (radio_access_technology_element->radio_interface)
      {
      case QMI_NAS_RADIO_INTERFACE_CDMA_1X:
        resp.list[i].act = NETWORK_ACT_CDMA_1X; break;

      case QMI_NAS_RADIO_INTERFACE_CDMA_1XEVDO:
        resp.list[i].act = NETWORK_ACT_CDMA_1X_EVDO_REV0; break;

      case QMI_NAS_RADIO_INTERFACE_GSM:
        resp.list[i].act = NETWORK_ACT_GSM; break;

      case QMI_NAS_RADIO_INTERFACE_UMTS:
        resp.list[i].act = NETWORK_ACT_UMTS; break;

      case QMI_NAS_RADIO_INTERFACE_LTE:
        resp.list[i].act = NETWORK_ACT_LTE; break;

      case QMI_NAS_RADIO_INTERFACE_5GNR:
        resp.list[i].act = NETWORK_ACT_NR; break;

      case QMI_NAS_RADIO_INTERFACE_UNKNOWN:
      case QMI_NAS_RADIO_INTERFACE_NONE:
      default:
        resp.list[i].act = NETWORK_ACT_NOT_SPECIFIED; break;
      }
    }
  }
  else
  {
    resp.result = TCORE_RETURN_FAILURE;
  }

  tcore_user_request_send_response(ur,
      TRESP_NETWORK_SEARCH,
      sizeof(resp), &resp);

  qmi_message_nas_network_scan_output_unref(output);
}

static TReturn search_network(CoreObject *co_network, UserRequest *ur)
{
  PluginData *user_data =
      tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_network));

  qmi_client_nas_network_scan(user_data->nas_client,
      NULL,
      300,
      user_data->cancellable,
      (GAsyncReadyCallback) search_network_ready,
      ur);

  return TCORE_RETURN_SUCCESS;
}

static TReturn set_band(CoreObject *co_network, UserRequest *ur)
{
  struct tresp_network_set_band resp = { };

  resp.result = TCORE_RETURN_FAILURE;

  tcore_user_request_send_response(
    ur, TRESP_NETWORK_SET_BAND, sizeof(resp), &resp);

  return TCORE_RETURN_SUCCESS;
}

static void get_band_ready(QmiClientNas *client,
    GAsyncResult *res, UserRequest *ur)
{
  QmiMessageNasGetRfBandInformationOutput *output;

  struct tresp_network_get_band resp = { };

  output = qmi_client_nas_get_rf_band_information_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_nas_get_rf_band_information_output_get_result(output, NULL))
  {
    GArray *band_information;

    resp.result = TCORE_RETURN_SUCCESS;

    qmi_message_nas_get_rf_band_information_output_get_list(output,
        &band_information, NULL);
    if (band_information->len > 0)
    {
      QmiMessageNasGetRfBandInformationOutputListElement *band_information_element;

      band_information_element = &g_array_index(
        band_information,
        QmiMessageNasGetRfBandInformationOutputListElement, 0);

      switch (band_information_element->radio_interface)
      {
      case QMI_NAS_RADIO_INTERFACE_CDMA_1X:
      case QMI_NAS_RADIO_INTERFACE_CDMA_1XEVDO:
      case QMI_NAS_RADIO_INTERFACE_UMTS:
        resp.band = NETWORK_BAND_TYPE_WCDMA; break;

      case QMI_NAS_RADIO_INTERFACE_GSM:
        resp.band = NETWORK_BAND_TYPE_GSM_900_1800; break;

      case QMI_NAS_RADIO_INTERFACE_LTE:
        resp.band = NETWORK_BAND_TYPE_LTE_BAND_4; break;

      case QMI_NAS_RADIO_INTERFACE_UNKNOWN:
      case QMI_NAS_RADIO_INTERFACE_NONE:
      default:
        resp.band = NETWORK_BAND_TYPE_ANY; break;
      }
    }

    resp.mode = NETWORK_BAND_MODE_PREFERRED;
  }
  else
  {
    resp.result = TCORE_RETURN_FAILURE;
  }

  tcore_user_request_send_response(ur, TRESP_NETWORK_GET_BAND,
      sizeof(resp), &resp);

  qmi_message_nas_get_rf_band_information_output_unref(output);
}

static TReturn get_band(CoreObject *co_network, UserRequest *ur)
{
  PluginData *user_data =
      tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_network));

  qmi_client_nas_get_rf_band_information(user_data->nas_client,
      NULL,
      300,
      user_data->cancellable,
      (GAsyncReadyCallback) get_band_ready,
      ur);

  return TCORE_RETURN_SUCCESS;
}

static void set_service_domain_ready(QmiClientNas *client,
    GAsyncResult *res, UserRequest *ur)
{
  QmiMessageNasSetSystemSelectionPreferenceOutput *output;

  struct tresp_network_set_service_domain resp = { };

  output = qmi_client_nas_set_system_selection_preference_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_nas_set_system_selection_preference_output_get_result(output, NULL))
  {
    resp.result = TCORE_RETURN_SUCCESS;
  }
  else
  {
    resp.result = TCORE_RETURN_FAILURE;
  }

  tcore_user_request_send_response(ur,
      TRESP_NETWORK_SET_SERVICE_DOMAIN,
      sizeof(resp), &resp);

  qmi_message_nas_set_system_selection_preference_output_unref(output);
}

static TReturn set_service_domain(CoreObject *co_network, UserRequest *ur)
{
  PluginData *user_data =
      tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_network));
  struct treq_network_set_service_domain *req =
      (struct treq_network_set_service_domain *) tcore_user_request_ref_data(ur, NULL);

  QmiMessageNasSetSystemSelectionPreferenceInput *input;
  QmiNasServiceDomainPreference service_domain_preference;

  input = qmi_message_nas_set_system_selection_preference_input_new();

  switch (req->domain)
  {
  case NETWORK_SERVICE_DOMAIN_CS:
    service_domain_preference = QMI_NAS_SERVICE_DOMAIN_PREFERENCE_CS_ONLY;
    break;

  case NETWORK_SERVICE_DOMAIN_PS:
    service_domain_preference = QMI_NAS_SERVICE_DOMAIN_PREFERENCE_PS_ONLY;
    break;

  case NETWORK_SERVICE_DOMAIN_COMBINED:
    service_domain_preference = QMI_NAS_SERVICE_DOMAIN_PREFERENCE_CS_PS;
    break;

  case NETWORK_SERVICE_DOMAIN_AUTOMATIC:
  default:
    service_domain_preference = QMI_NAS_SERVICE_DOMAIN_PREFERENCE_CS_PS;
    break;
  }

  qmi_message_nas_set_system_selection_preference_input_set_service_domain_preference(input,
      service_domain_preference, NULL);

  qmi_client_nas_set_system_selection_preference(user_data->nas_client,
      input,
      300,
      user_data->cancellable,
      (GAsyncReadyCallback) set_service_domain_ready,
      ur);

  qmi_message_nas_set_system_selection_preference_input_unref(input);

  return TCORE_RETURN_SUCCESS;
}

static void get_service_domain_ready(QmiClientNas *client,
    GAsyncResult *res, UserRequest *ur)
{
  QmiMessageNasGetSystemSelectionPreferenceOutput *output;

  QmiNasServiceDomainPreference service_domain_preference;
  struct tresp_network_get_service_domain resp = { };

  output = qmi_client_nas_get_system_selection_preference_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_nas_get_system_selection_preference_output_get_result(output, NULL) &&
      qmi_message_nas_get_system_selection_preference_output_get_service_domain_preference(output, &service_domain_preference, NULL))
  {
    resp.result = TCORE_RETURN_SUCCESS;

    switch (service_domain_preference)
    {
    case QMI_NAS_SERVICE_DOMAIN_PREFERENCE_CS_ONLY:
      resp.domain = NETWORK_SERVICE_DOMAIN_CS; break;

    case QMI_NAS_SERVICE_DOMAIN_PREFERENCE_PS_ONLY:
      resp.domain = NETWORK_SERVICE_DOMAIN_PS; break;

    case QMI_NAS_SERVICE_DOMAIN_PREFERENCE_CS_PS:
      resp.domain = NETWORK_SERVICE_DOMAIN_COMBINED; break;

    default:
      resp.domain = NETWORK_SERVICE_DOMAIN_AUTOMATIC; break;
    }
  }
  else
  {
    resp.result = TCORE_RETURN_FAILURE;
  }

  tcore_user_request_send_response(ur,
      TRESP_NETWORK_GET_SERVICE_DOMAIN,
      sizeof(resp), &resp);

  qmi_message_nas_get_system_selection_preference_output_unref(output);
}

static TReturn get_service_domain(CoreObject *co_network, UserRequest *ur)
{
  PluginData *user_data =
      tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_network));

  qmi_client_nas_get_system_selection_preference(user_data->nas_client,
      NULL,
      300,
      user_data->cancellable,
      (GAsyncReadyCallback) get_service_domain_ready,
      ur);

  return TCORE_RETURN_SUCCESS;
}


static TReturn set_plmn_selection_mode(CoreObject *co_network, UserRequest *ur)
{
  TReturn ret = TCORE_RETURN_SUCCESS;
  struct tresp_network_set_plmn_selection_mode mode = { };

  mode.result = TCORE_RETURN_SUCCESS;

  tcore_user_request_send_response(ur,
      TRESP_NETWORK_SET_PLMN_SELECTION_MODE,
      sizeof(mode), &mode);

  return ret;
}

static TReturn get_plmn_selection_mode(CoreObject *co_network, UserRequest *ur)
{
  TReturn ret = TCORE_RETURN_SUCCESS;
  struct tresp_network_get_plmn_selection_mode mode = { };

  mode.result = TCORE_RETURN_SUCCESS;
  mode.mode = NETWORK_SELECT_MODE_AUTOMATIC;

  tcore_user_request_send_response(ur,
      TRESP_NETWORK_GET_PLMN_SELECTION_MODE,
      sizeof(mode), &mode);

  return ret;
}

static TReturn set_mode(CoreObject *co_network, UserRequest *ur)
{
  TReturn ret = TCORE_RETURN_SUCCESS;
  struct tresp_network_set_mode mode = { };

  mode.result = TCORE_RETURN_SUCCESS;

  tcore_user_request_send_response(ur,
      TRESP_NETWORK_SET_MODE,
      sizeof(mode), &mode);

  return ret;
}

static TReturn get_mode(CoreObject *co_network, UserRequest *ur)
{
  TReturn ret = TCORE_RETURN_SUCCESS;
  struct tresp_network_get_mode mode = { };

  mode.result = TCORE_RETURN_SUCCESS;
  mode.mode = NETWORK_MODE_AUTO;

  tcore_user_request_send_response(ur,
      TRESP_NETWORK_GET_MODE,
      sizeof(mode), &mode);

  return ret;
}

static TReturn get_serving_network(CoreObject *co_network, UserRequest *ur)
{
  TReturn ret = TCORE_RETURN_SUCCESS;
  struct tresp_network_get_serving_network serving_network = { };

  serving_network.result = TCORE_RETURN_SUCCESS;
  strcpy(serving_network.plmn, tcore_network_get_plmn(co_network));
  tcore_network_get_access_technology(co_network, &serving_network.act);

  tcore_user_request_send_response(
    ur, TRESP_NETWORK_GET_SERVING_NETWORK,
    sizeof(serving_network), &serving_network);

  return ret;
}

static TReturn get_default_subscription(CoreObject *co, UserRequest *ur)
{
  struct tresp_network_get_default_subs resp = { };
  TcorePlugin *plugin = tcore_object_ref_plugin(co);
  Server *server = tcore_plugin_ref_server(plugin);
  Storage *strg = tcore_server_find_storage(server, "vconf");
  TReturn ret;

  resp.default_subs = tcore_storage_get_int(strg,
      STORAGE_KEY_TELEPHONY_DUALSIM_DEFAULT_SERVICE_INT);
  resp.result = TCORE_RETURN_SUCCESS;

  ret = tcore_user_request_send_response(ur,
      TRESP_NETWORK_GET_DEFAULT_SUBSCRIPTION,
      sizeof(resp), &resp);

  return ret;
}

static TReturn get_default_data_subscription(CoreObject *co, UserRequest *ur)
{
  struct tresp_network_get_default_data_subs resp = { };
  TcorePlugin *plugin = tcore_object_ref_plugin(co);
  Server *server = tcore_plugin_ref_server(plugin);
  Storage *strg = tcore_server_find_storage(server, "vconf");
  TReturn ret;

  resp.default_subs = tcore_storage_get_int(strg,
      STORAGE_KEY_TELEPHONY_DUALSIM_DEFAULT_DATA_SERVICE_INT);
  resp.result = TCORE_RETURN_SUCCESS;

  ret = tcore_user_request_send_response(ur,
      TRESP_NETWORK_GET_DEFAULT_DATA_SUBSCRIPTION,
      sizeof(resp), &resp);

  return ret;
}

static void handle_signal_strength(PluginData *user_data, gint8 rssi)
{
  CoreObject *co_network = tcore_plugin_ref_core_object(user_data->plugin, CORE_OBJECT_TYPE_NETWORK);
  Server *s = tcore_plugin_ref_server(user_data->plugin);

  struct tnoti_network_signal_strength signal_strength = { };
  struct tnoti_network_icon_info icon_info = { };

  signal_strength.dbm = rssi;

  tcore_server_send_notification(s,
      co_network, TNOTI_NETWORK_SIGNAL_STRENGTH,
      sizeof(signal_strength), &signal_strength);

  icon_info.type = NETWORK_ICON_INFO_RSSI;
  if (rssi <= -120) { icon_info.rssi = 0; }
  else if (rssi <= -110) { icon_info.rssi = 1; }
  else if (rssi <= -90) { icon_info.rssi = 2; }
  else if (rssi <= -80) { icon_info.rssi = 3; }
  else if (rssi <= -70) { icon_info.rssi = 4; }
  else if (rssi <= -60) { icon_info.rssi = 5; }
  else { icon_info.rssi = 6; }

  tcore_server_send_notification(s,
      co_network, TNOTI_NETWORK_ICON_INFO,
      sizeof(icon_info), &icon_info);
}

static void event_report(QmiClientNas *client,
    QmiIndicationNasEventReportOutput *output, PluginData *user_data)
{
  guint8 rssi;
  QmiNasRadioInterface radio_interface;

  if (qmi_indication_nas_event_report_output_get_rssi(output, &rssi, &radio_interface, NULL))
  {
    handle_signal_strength(user_data, -rssi);
  }
}

static void set_event_report_ready(QmiClientNas *client,
    GAsyncResult *res, PluginData *user_data)
{
  QmiMessageNasSetEventReportOutput *output;

  output = qmi_client_nas_set_event_report_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_nas_set_event_report_output_get_result(output, NULL))
  {
    user_data->nas_event_report_id = g_signal_connect(client,
        "event-report", G_CALLBACK(event_report), user_data);
  }

  qmi_message_nas_set_event_report_output_unref(output);
}

static void handle_network_change_notification(PluginData *user_data,
    guint16 current_plmn_mcc, guint16 current_plmn_mnc,
    const gchar *current_plmn_description, GArray *radio_interface_list)
{
  CoreObject *co_network = tcore_plugin_ref_core_object(user_data->plugin,
      CORE_OBJECT_TYPE_NETWORK);
  Server *s = tcore_plugin_ref_server(user_data->plugin);

  struct tnoti_network_change network_change = { };

  if ((radio_interface_list != NULL) && radio_interface_list->len != 0)
  {
    switch (g_array_index(radio_interface_list, QmiNasRadioInterface, 0))
    {
    case QMI_NAS_RADIO_INTERFACE_NONE:
      network_change.act = NETWORK_ACT_NOT_SPECIFIED;
      break;

    case QMI_NAS_RADIO_INTERFACE_AMPS:
    case QMI_NAS_RADIO_INTERFACE_GSM:
      network_change.act = NETWORK_ACT_GSM;
      break;

    case QMI_NAS_RADIO_INTERFACE_CDMA_1X:
    case QMI_NAS_RADIO_INTERFACE_CDMA_1XEVDO:
    case QMI_NAS_RADIO_INTERFACE_UMTS:
      network_change.act = NETWORK_ACT_UMTS;
      break;

    case QMI_NAS_RADIO_INTERFACE_LTE:
      network_change.act = NETWORK_ACT_LTE;
      break;

    case QMI_NAS_RADIO_INTERFACE_TD_SCDMA:
    case QMI_NAS_RADIO_INTERFACE_5GNR:
      network_change.act = NETWORK_ACT_NR;
      break;

    case QMI_NAS_RADIO_INTERFACE_UNKNOWN:
    default:
      network_change.act = NETWORK_ACT_UNKNOWN;
    }

    tcore_network_set_access_technology(co_network, network_change.act);
  }
  else
  {
    tcore_network_get_access_technology(co_network, &network_change.act);
  }

  sprintf(network_change.plmn, "%03u%02u",
      current_plmn_mcc % 1000, current_plmn_mnc % 100);

  tcore_network_set_plmn(co_network, network_change.plmn);
  tcore_network_set_network_name(co_network, TCORE_NETWORK_NAME_TYPE_FULL,
      current_plmn_description);

  tcore_server_send_notification(s,
      co_network, TNOTI_NETWORK_CHANGE,
      sizeof(network_change), &network_change);
}

static void handle_network_registration_notification(PluginData *user_data,
    QmiNasRegistrationState registration_state,
    QmiNasAttachState cs_attach_state, QmiNasAttachState ps_attach_state,
    QmiNasNetworkType selected_network, GArray *radio_interface_list,
    QmiNasRoamingIndicatorStatus roaming_indicator)
{
  CoreObject *co_network = tcore_plugin_ref_core_object(user_data->plugin,
      CORE_OBJECT_TYPE_NETWORK);
  Server *s = tcore_plugin_ref_server(user_data->plugin);

  struct tnoti_network_registration_status reg_status = { };
  struct tnoti_network_icon_info icon_info = { };

  if (radio_interface_list->len != 0)
  {
    switch (g_array_index(radio_interface_list, QmiNasRadioInterface, 0))
    {
    case QMI_NAS_RADIO_INTERFACE_NONE:
      reg_status.service_type = NETWORK_SERVICE_TYPE_NO_SERVICE;
      break;

    case QMI_NAS_RADIO_INTERFACE_AMPS:
    case QMI_NAS_RADIO_INTERFACE_GSM:
      reg_status.service_type = NETWORK_SERVICE_TYPE_2G;
      break;

    case QMI_NAS_RADIO_INTERFACE_CDMA_1X:
    case QMI_NAS_RADIO_INTERFACE_CDMA_1XEVDO:
    case QMI_NAS_RADIO_INTERFACE_UMTS:
      reg_status.service_type = NETWORK_SERVICE_TYPE_3G;
      break;

    case QMI_NAS_RADIO_INTERFACE_LTE:
      reg_status.service_type = NETWORK_SERVICE_TYPE_LTE;
      break;

    case QMI_NAS_RADIO_INTERFACE_TD_SCDMA:
    case QMI_NAS_RADIO_INTERFACE_5GNR:
      reg_status.service_type = NETWORK_SERVICE_TYPE_5G;
      break;

    case QMI_NAS_RADIO_INTERFACE_UNKNOWN:
    default:
      reg_status.service_type = NETWORK_SERVICE_TYPE_UNKNOWN;
    }

    tcore_network_set_service_type(co_network, reg_status.service_type);
  }
  else
  {
    tcore_network_get_service_type(co_network, &reg_status.service_type);
  }

  switch (cs_attach_state)
  {
  case QMI_NAS_ATTACH_STATE_ATTACHED:
    reg_status.cs_domain_status = NETWORK_SERVICE_DOMAIN_STATUS_FULL;
    break;

  case QMI_NAS_ATTACH_STATE_DETACHED:
    reg_status.cs_domain_status = NETWORK_SERVICE_DOMAIN_STATUS_NO;
    break;

  case QMI_NAS_ATTACH_STATE_UNKNOWN:
  default:
    reg_status.cs_domain_status = NETWORK_SERVICE_DOMAIN_STATUS_SEARCH;
    break;
  }

  tcore_network_set_service_status(co_network,
      TCORE_NETWORK_SERVICE_DOMAIN_TYPE_CIRCUIT, reg_status.cs_domain_status);

  switch (ps_attach_state)
  {
  case QMI_NAS_ATTACH_STATE_ATTACHED:
    reg_status.ps_domain_status = NETWORK_SERVICE_DOMAIN_STATUS_FULL;
    break;

  case QMI_NAS_ATTACH_STATE_DETACHED:
    reg_status.ps_domain_status = NETWORK_SERVICE_DOMAIN_STATUS_NO;
    break;

  case QMI_NAS_ATTACH_STATE_UNKNOWN:
  default:
    reg_status.ps_domain_status = NETWORK_SERVICE_DOMAIN_STATUS_SEARCH;
    break;
  }

  tcore_network_set_service_status(co_network,
      TCORE_NETWORK_SERVICE_DOMAIN_TYPE_PACKET, reg_status.ps_domain_status);

  switch (roaming_indicator)
  {
  case QMI_NAS_ROAMING_INDICATOR_STATUS_ON:
    reg_status.roaming_status = TRUE;
    break;

  case QMI_NAS_ROAMING_INDICATOR_STATUS_OFF:
  default:
    reg_status.roaming_status = FALSE;
  }

  tcore_network_set_roaming_state(co_network, reg_status.roaming_status);

  tcore_server_send_notification(s,
      co_network, TNOTI_NETWORK_REGISTRATION_STATUS,
      sizeof(reg_status), &reg_status);

  icon_info.type = NETWORK_ICON_INFO_ROAM_ICON_MODE;
  icon_info.roam_icon_mode = reg_status.roaming_status;

  tcore_server_send_notification(s,
      co_network, TNOTI_NETWORK_ICON_INFO,
      sizeof(icon_info), &icon_info);
}

static void handle_lac_cid(PluginData *user_data,
    guint16 lac, guint32 cid)
{
  CoreObject *co_network = tcore_plugin_ref_core_object(user_data->plugin,
      CORE_OBJECT_TYPE_NETWORK);
  Server *s = tcore_plugin_ref_server(user_data->plugin);

  struct tnoti_network_location_cellinfo cellinfo = { };

  cellinfo.lac = lac;
  cellinfo.cell_id = cid;
  cellinfo.physical_cell_id = cid;

  tcore_network_set_lac(co_network, lac);
  tcore_network_set_cell_id(co_network, cid);
  tcore_network_set_physical_cell_id(co_network, cid);

  tcore_server_send_notification(s,
      co_network, TNOTI_NETWORK_LOCATION_CELLINFO,
      sizeof(cellinfo), &cellinfo);
}

static void serving_system(QmiClientNas *client,
    QmiIndicationNasServingSystemOutput *output, PluginData *user_data)
{
  CoreObject *co_network = tcore_plugin_ref_core_object(user_data->plugin,
      CORE_OBJECT_TYPE_NETWORK);
  Server *s = tcore_plugin_ref_server(user_data->plugin);

  guint16 current_plmn_mcc;
  guint16 current_plmn_mnc;
  const gchar *current_plmn_description;

  QmiNasRegistrationState registration_state;
  QmiNasAttachState cs_attach_state;
  QmiNasAttachState ps_attach_state;
  QmiNasNetworkType selected_network;
  GArray *radio_interface_list = NULL;

  gboolean has_serving_system;
  QmiNasRoamingIndicatorStatus roaming_indicator;

  guint16 year;
  guint8 month;
  guint8 day;
  guint8 hour;
  guint8 minute;
  guint8 second;
  guint8 time_zone;

  guint16 lac;
  guint32 cid;

  guint16 base_station_id;
  gint base_station_latitude;
  gint base_station_longitude;

  guint16 system_id_sid;
  guint16 system_id_nid;

  has_serving_system =
      qmi_indication_nas_serving_system_output_get_serving_system(output,
          &registration_state, &cs_attach_state, &ps_attach_state,
          &selected_network, &radio_interface_list, NULL);

  if (qmi_indication_nas_serving_system_output_get_current_plmn(output,
          &current_plmn_mcc, &current_plmn_mnc, &current_plmn_description,
          NULL))
  {
    handle_network_change_notification(user_data,
        current_plmn_mcc, current_plmn_mnc, current_plmn_description,
        radio_interface_list);
  }

  if (has_serving_system)
  {
    GArray *roaming_indicator_list = NULL;

    if (qmi_indication_nas_serving_system_output_get_roaming_indicator(output,
            &roaming_indicator, NULL))
    {
      handle_network_registration_notification(user_data,
          registration_state, cs_attach_state, ps_attach_state, selected_network,
          radio_interface_list, roaming_indicator);
    }
    else if (qmi_indication_nas_serving_system_output_get_roaming_indicator_list(output,
            &roaming_indicator_list, NULL) &&
        (roaming_indicator_list->len != 0))
    {
      roaming_indicator =
          g_array_index(roaming_indicator_list,
              QmiIndicationNasServingSystemOutputRoamingIndicatorListElement,
              0).roaming_indicator;

      handle_network_registration_notification(user_data,
          registration_state, cs_attach_state, ps_attach_state, selected_network,
          radio_interface_list, roaming_indicator);
    }
  }

  if (qmi_indication_nas_serving_system_output_get_universal_time_and_local_time_zone_3gpp(output,
          &year, &month, &day, &hour, &minute, &second, &time_zone, NULL))
  {
    struct tnoti_network_timeinfo timeinfo = {};

    timeinfo.year = year - 2000;
    timeinfo.month = month;
    timeinfo.day = day;
    timeinfo.hour = hour;
    timeinfo.minute = minute;
    timeinfo.second = second;
    timeinfo.gmtoff = (time_zone & 0x7f) * 60 / 4;
    if ((time_zone & 0x80) != 0)
    {
      timeinfo.gmtoff *= -1;
    }

    tcore_server_send_notification(s,
        co_network, TNOTI_NETWORK_TIMEINFO,
        sizeof(timeinfo), &timeinfo);
  }

  if (qmi_indication_nas_serving_system_output_get_lac_3gpp(output,
          &lac, NULL) &&
      qmi_indication_nas_serving_system_output_get_cid_3gpp(output,
          &cid, NULL))
  {
    handle_lac_cid(user_data, lac, cid);
  }

  if (qmi_indication_nas_serving_system_output_get_cdma_base_station_info(output,
          &base_station_id, &base_station_latitude, &base_station_longitude,
          NULL) &&
      qmi_indication_nas_serving_system_output_get_cdma_system_id(output,
          &system_id_sid, &system_id_nid, NULL))
  {
    struct tnoti_network_location_cellinfo_cdma cellinfo = {};

    cellinfo.system_id = system_id_sid;
    cellinfo.network_id = system_id_nid;
    cellinfo.bs_id = base_station_id;
    cellinfo.bs_latitude = base_station_latitude;
    cellinfo.bs_longitude = base_station_longitude;

    tcore_server_send_notification(s,
        co_network, TNOTI_NETWORK_LOCATION_CELLINFO_CDMA,
        sizeof(cellinfo), &cellinfo);
  }
}

static void get_serving_system_ready_init(QmiClientNas *client,
    GAsyncResult *res, PluginData *user_data)
{
  QmiMessageNasGetServingSystemOutput *output;

  output = qmi_client_nas_get_serving_system_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_nas_get_serving_system_output_get_result(output, NULL))
  {
    guint16 current_plmn_mcc;
    guint16 current_plmn_mnc;
    const gchar *current_plmn_description;

    QmiNasRegistrationState registration_state;
    QmiNasAttachState cs_attach_state;
    QmiNasAttachState ps_attach_state;
    QmiNasNetworkType selected_network;
    GArray *radio_interface_list;

    QmiNasRoamingIndicatorStatus roaming_indicator;

    guint16 lac;
    guint32 cid;

    if (qmi_message_nas_get_serving_system_output_get_current_plmn(output,
            &current_plmn_mcc, &current_plmn_mnc, &current_plmn_description,
            NULL) &&
        qmi_message_nas_get_serving_system_output_get_serving_system(output,
            &registration_state, &cs_attach_state, &ps_attach_state,
            &selected_network, &radio_interface_list, NULL) &&
        qmi_message_nas_get_serving_system_output_get_roaming_indicator(output,
            &roaming_indicator, NULL))
    {
      handle_network_change_notification(user_data,
          current_plmn_mcc, current_plmn_mnc, current_plmn_description,
          radio_interface_list);

      handle_network_registration_notification(user_data,
          registration_state, cs_attach_state, ps_attach_state, selected_network,
          radio_interface_list, roaming_indicator);
    }

    if (qmi_message_nas_get_serving_system_output_get_lac_3gpp(output,
            &lac, NULL) &&
        qmi_message_nas_get_serving_system_output_get_cid_3gpp(output,
            &cid, NULL))
    {
      handle_lac_cid(user_data, lac, cid);
    }
  }

  qmi_message_nas_get_serving_system_output_unref(output);
}

static void get_signal_strength_ready_init(QmiClientNas *client,
    GAsyncResult *res, PluginData *user_data)
{
  QmiMessageNasGetSignalStrengthOutput *output;

  gint8 signal_strength;
  QmiNasRadioInterface radio_interface;

  output = qmi_client_nas_get_signal_strength_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_nas_get_signal_strength_output_get_result(output, NULL) &&
      qmi_message_nas_get_signal_strength_output_get_signal_strength(output,
          &signal_strength, &radio_interface, NULL))
  {
    handle_signal_strength(user_data, signal_strength);
  }

  qmi_message_nas_get_signal_strength_output_unref(output);
}

static void register_indications_ready(QmiClientNas *client,
    GAsyncResult *res, PluginData *user_data)
{
  QmiMessageNasRegisterIndicationsOutput *output;

  output = qmi_client_nas_register_indications_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_nas_register_indications_output_get_result(output, NULL))
  {
    user_data->nas_serving_system_id = g_signal_connect(client,
        "serving-system", G_CALLBACK(serving_system), user_data);
  }

  qmi_message_nas_register_indications_output_unref(output);

  qmi_client_nas_get_serving_system(user_data->nas_client,
      NULL,
      15,
      user_data->cancellable,
      (GAsyncReadyCallback) get_serving_system_ready_init,
      user_data);

  qmi_client_nas_get_signal_strength(user_data->nas_client,
      NULL,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) get_signal_strength_ready_init,
      user_data);
}

static void allocate_client_ready(QmiDevice *dev, GAsyncResult *res,
    PluginData *user_data)
{
  CoreObject *co_network;
  QmiClient *client;

  co_network = tcore_plugin_ref_core_object(user_data->plugin, CORE_OBJECT_TYPE_NETWORK);

  client = qmi_device_allocate_client_finish(dev, res, NULL);
  user_data->nas_client = QMI_CLIENT_NAS(client);

  {
    QmiMessageNasSetEventReportInput *input =
        qmi_message_nas_set_event_report_input_new();

    qmi_message_nas_set_event_report_input_set_rssi_indicator(input,
        TRUE, 5, NULL);
    qmi_client_nas_set_event_report(user_data->nas_client,
        input,
        10,
        user_data->cancellable,
        (GAsyncReadyCallback) set_event_report_ready,
        user_data);
    qmi_message_nas_set_event_report_input_unref(input);
  }

  {
    QmiMessageNasRegisterIndicationsInput *input =
        qmi_message_nas_register_indications_input_new();

    qmi_message_nas_register_indications_input_set_serving_system_events(input,
        TRUE, NULL);
    qmi_client_nas_register_indications(user_data->nas_client,
        input,
        10,
        user_data->cancellable,
        (GAsyncReadyCallback) register_indications_ready,
        user_data);
    qmi_message_nas_register_indications_input_unref(input);
  }

  tcore_network_set_service_type(co_network, NETWORK_SERVICE_TYPE_UNKNOWN);

  user_data->network_inited = TRUE;
}

static void release_client_ready(QmiDevice *dev, GAsyncResult *res,
    PluginData *user_data)
{
  qmi_device_release_client_finish(dev, res, NULL);
  user_data->nas_client = NULL;
}


/** Network operations */
static struct tcore_network_operations network_ops = {
  .search = search_network,
  .set_plmn_selection_mode = set_plmn_selection_mode,
  .get_plmn_selection_mode = get_plmn_selection_mode,
  .set_service_domain = set_service_domain,
  .get_service_domain = get_service_domain,
  .set_band = set_band,
  .get_band = get_band,
  .set_preferred_plmn = NULL,
  .get_preferred_plmn = NULL,
  .set_order = NULL,
  .get_order = NULL,
  .set_power_on_attach = NULL,
  .get_power_on_attach = NULL,
  .set_cancel_manual_search = NULL,
  .get_serving_network = get_serving_network,
  .set_mode = set_mode,
  .get_mode = get_mode,
  .get_default_subscription = get_default_subscription,
  .get_default_data_subscription = get_default_data_subscription,
};

gboolean s_network_init(TcorePlugin *p, PluginData *user_data)
{
  CoreObject *co_network;

  co_network = tcore_network_new(p, "umts_network", &network_ops, NULL);
  if (!co_network)
  {
    err("Core object is NULL");
    return FALSE;
  }

  qmi_device_allocate_client(user_data->device,
      QMI_SERVICE_NAS,
      QMI_CID_NONE,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) allocate_client_ready,
      user_data);

  return TRUE;
}

void s_network_exit(TcorePlugin *p)
{
  PluginData *user_data = tcore_plugin_ref_user_data(p);
  CoreObject *co_network = tcore_plugin_ref_core_object(p, CORE_OBJECT_TYPE_NETWORK);

  if (user_data->nas_event_report_id)
  {
    g_signal_handler_disconnect(user_data->nas_client,
        user_data->nas_event_report_id);
  }

  if (user_data->nas_serving_system_id)
  {
    g_signal_handler_disconnect(user_data->nas_client,
        user_data->nas_serving_system_id);
  }

  qmi_device_release_client(user_data->device,
      QMI_CLIENT(user_data->nas_client),
      QMI_DEVICE_RELEASE_CLIENT_FLAGS_NONE,
      10,
      NULL,
      (GAsyncReadyCallback) release_client_ready,
      NULL);

  tcore_network_free(co_network);
}
