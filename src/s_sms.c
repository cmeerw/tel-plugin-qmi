/*
 * tel-plugin-qmi
 *
 * Copyright (c) 2012 Samsung Electronics Co., Ltd. All rights reserved.
 * Copyright (c) 2021 Christof Meerwald. All rights reserved.
 *
 * Contact: Hayoon Ko <hayoon.ko@samsung.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>

#include <tzplatform_config.h>
#include <tcore.h>
#include <user_request.h>
#include <core_object.h>
#include <plugin.h>
#include <server.h>
#include <co_sms.h>

#include "s_common.h"
#include "s_sms.h"


static void raw_send_ready(QmiClientWms *client, GAsyncResult *res,
    UserRequest *ur)
{
  GError *error = NULL;
  QmiMessageWmsRawSendOutput *output;
  struct tresp_sms_send_msg resp = { };

  resp.result = TCORE_RETURN_FAILURE;

  output = qmi_client_wms_raw_send_finish(client, res, &error);
  if (!output)
  {
    g_error("error: couldn't send message: %s\n", error->message);
  }
  else if (qmi_message_wms_raw_send_output_get_result(output, NULL))
  {
    resp.result = TCORE_RETURN_SUCCESS;
  }

  tcore_user_request_send_response(ur, TRESP_SMS_SEND_UMTS_MSG,
      sizeof(resp), &resp);

  qmi_message_wms_raw_send_output_unref(output);
}

static TReturn send_umts_msg(CoreObject *co_sms, UserRequest *ur)
{
  PluginData *user_data;
  QmiMessageWmsRawSendInput *input;
  struct treq_sms_send_msg *send_msg_req;
  unsigned sca_len;

  GArray *raw_data;

  user_data = tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_sms));
  send_msg_req = (struct treq_sms_send_msg *) tcore_user_request_ref_data(ur, NULL);
  sca_len = send_msg_req->msgDataPackage.sca[0] + 1;

  input = qmi_message_wms_raw_send_input_new();

  raw_data = g_array_sized_new(FALSE, FALSE, 1,
      sca_len + send_msg_req->msgDataPackage.msgLength);
  g_array_append_vals(raw_data, send_msg_req->msgDataPackage.sca, sca_len);
  g_array_append_vals(raw_data,
      send_msg_req->msgDataPackage.tpduData,
      send_msg_req->msgDataPackage.msgLength);
  qmi_message_wms_raw_send_input_set_raw_message_data(input,
      QMI_WMS_MESSAGE_FORMAT_GSM_WCDMA_POINT_TO_POINT, raw_data, NULL);

  qmi_client_wms_raw_send(user_data->wms_client,
      input,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) raw_send_ready,
      ur);

  qmi_message_wms_raw_send_input_unref(input);
  g_array_unref(raw_data);

  return TCORE_RETURN_SUCCESS;
}

static void raw_read_ready(QmiClientWms *client, GAsyncResult *res,
    UserRequest *ur)
{
  GError *error = NULL;
  QmiMessageWmsRawReadOutput *output;
  struct tresp_sms_read_msg resp = { };

  QmiWmsMessageTagType message_tag;
  QmiWmsMessageFormat data_format;
  GArray *raw_data;

  resp.result = TCORE_RETURN_FAILURE;

  output = qmi_client_wms_raw_read_finish(client, res, &error);
  if (!output)
  {
    g_error("error: couldn't read message: %s\n", error->message);
  }
  else if (qmi_message_wms_raw_read_output_get_result(output, NULL) &&
      qmi_message_wms_raw_read_output_get_raw_message_data(output,
          &message_tag, &data_format, &raw_data, NULL))
  {
    resp.result = TCORE_RETURN_SUCCESS;
  }

  tcore_user_request_send_response(ur, TRESP_SMS_READ_MSG,
      sizeof(resp), &resp);

  qmi_message_wms_raw_read_output_unref(output);

  return;
}

static TReturn read_msg(CoreObject *co_sms, UserRequest *ur)
{
  PluginData *user_data;
  QmiMessageWmsRawReadInput *input;
  struct treq_sms_read_msg *read_msg_req;

  user_data = tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_sms));
  read_msg_req = (struct treq_sms_read_msg *) tcore_user_request_ref_data(ur, NULL);

  input = qmi_message_wms_raw_read_input_new();

  qmi_message_wms_raw_read_input_set_message_mode(input, QMI_WMS_MESSAGE_MODE_GSM_WCDMA, NULL);
  qmi_message_wms_raw_read_input_set_sms_on_ims(input, FALSE, NULL);
  qmi_message_wms_raw_read_input_set_message_memory_storage_id(input, QMI_WMS_STORAGE_TYPE_NV, read_msg_req->index, NULL);

  qmi_client_wms_raw_read(user_data->wms_client,
      input,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) raw_read_ready,
      ur);

  qmi_message_wms_raw_read_input_unref(input);

  return TCORE_RETURN_SUCCESS;
}

static void delete_ready(QmiClientWms *client, GAsyncResult *res,
    UserRequest *ur)
{
  GError *error = NULL;
  QmiMessageWmsDeleteOutput *output;
  struct tresp_sms_delete_msg resp = { };

  resp.result = TCORE_RETURN_FAILURE;

  output = qmi_client_wms_delete_finish(client, res, &error);
  if (!output)
  {
    g_error("error: couldn't delete message: %s\n", error->message);
  }
  else if (qmi_message_wms_delete_output_get_result(output, NULL))
  {
    resp.result = TCORE_RETURN_SUCCESS;
  }

  tcore_user_request_send_response(ur, TRESP_SMS_DELETE_MSG,
      sizeof(resp), &resp);

  qmi_message_wms_delete_output_unref(output);

  return;
}

static TReturn delete_msg(CoreObject *co_sms, UserRequest *ur)
{
  PluginData *user_data;
  QmiMessageWmsDeleteInput *input;
  struct treq_sms_delete_msg *delete_msg_req;

  user_data = tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_sms));
  delete_msg_req = (struct treq_sms_delete_msg *) tcore_user_request_ref_data(ur, NULL);

  input = qmi_message_wms_delete_input_new();

  qmi_message_wms_delete_input_set_message_mode(input, QMI_WMS_MESSAGE_MODE_GSM_WCDMA, NULL);
  qmi_message_wms_delete_input_set_message_tag(input, QMI_WMS_MESSAGE_TAG_TYPE_MT_NOT_READ, NULL);
  qmi_message_wms_delete_input_set_memory_index(input, delete_msg_req->index, NULL);
  qmi_message_wms_delete_input_set_memory_storage(input, QMI_WMS_STORAGE_TYPE_NV, NULL);

  qmi_client_wms_delete(user_data->wms_client,
      input,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) delete_ready,
      ur);

  qmi_message_wms_delete_input_unref(input);

  return TCORE_RETURN_SUCCESS;
}

static void list_messages_ready(QmiClientWms *client, GAsyncResult *res,
    UserRequest *ur)
{
  GError *error = NULL;
  QmiMessageWmsListMessagesOutput *output;
  struct tresp_sms_get_storedMsgCnt count_info = { };

  GArray *message_list;

  count_info.result = TCORE_RETURN_FAILURE;

  output = qmi_client_wms_list_messages_finish(client, res, &error);
  if (!output)
  {
    g_error("error: couldn't get wms messages list: %s\n", error->message);
  }
  else if (qmi_message_wms_list_messages_output_get_result(output, NULL) &&
      qmi_message_wms_list_messages_output_get_message_list(output, &message_list, NULL))
  {
    guint i;

    count_info.result = TCORE_RETURN_SUCCESS;
    count_info.storedMsgCnt.totalCount = 0;
    count_info.storedMsgCnt.usedCount = 0;

    for (i = 0; i != message_list->len; ++i)
    {
      QmiMessageWmsListMessagesOutputMessageListElement *message_elem =
          &g_array_index(
            message_list,
            QmiMessageWmsListMessagesOutputMessageListElement, i);

      count_info.storedMsgCnt.indexList[i] = message_elem->memory_index;

      ++count_info.storedMsgCnt.totalCount;
      ++count_info.storedMsgCnt.usedCount;
    }
  }

  tcore_user_request_send_response(ur, TRESP_SMS_GET_STORED_MSG_COUNT,
      sizeof(count_info), &count_info);

  qmi_message_wms_list_messages_output_unref(output);

  return;
}

static TReturn get_msg_count(CoreObject *co_sms, UserRequest *ur)
{
  PluginData *user_data;
  QmiMessageWmsListMessagesInput *input;

  user_data = tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_sms));

  input = qmi_message_wms_list_messages_input_new();

  qmi_message_wms_list_messages_input_set_message_mode(input, QMI_WMS_MESSAGE_MODE_GSM_WCDMA, NULL);
  qmi_message_wms_list_messages_input_set_message_tag(input, QMI_WMS_MESSAGE_TAG_TYPE_MT_NOT_READ, NULL);
  qmi_message_wms_list_messages_input_set_storage_type(input, QMI_WMS_STORAGE_TYPE_NV, NULL);

  qmi_client_wms_list_messages(user_data->wms_client,
      input,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) list_messages_ready,
      ur);

  qmi_message_wms_list_messages_input_unref(input);

  return TCORE_RETURN_SUCCESS;
}

static void send_ack_ready(QmiClientWms *client, GAsyncResult *res,
    PluginData *user_data)
{
  QmiMessageWmsSendAckOutput *output;

  output = qmi_client_wms_send_ack_finish(client, res, NULL);

  qmi_message_wms_send_ack_output_unref(output);
}

static void event_report(QmiClientWms *client,
    QmiIndicationWmsEventReportOutput *output, PluginData *user_data)
{
  CoreObject *co_sms = tcore_plugin_ref_core_object(user_data->plugin, CORE_OBJECT_TYPE_SMS);
  Server *s = tcore_plugin_ref_server(user_data->plugin);

  QmiWmsAckIndicator ack_ind;
  guint32 transaction_id;
  QmiWmsMessageFormat msg_format;
  GArray *raw_data;

  if (qmi_indication_wms_event_report_output_get_transfer_route_mt_message(output,
          &ack_ind, &transaction_id, &msg_format, &raw_data, NULL))
  {
    struct tnoti_sms_incoming_msg incoming_msg = { };

    if (ack_ind == QMI_WMS_ACK_INDICATOR_SEND)
    {
      QmiMessageWmsSendAckInput *input = qmi_message_wms_send_ack_input_new();
      QmiWmsMessageProtocol message_protocol = QMI_WMS_MESSAGE_PROTOCOL_WCDMA;

      qmi_message_wms_send_ack_input_set_information(input,
          transaction_id, message_protocol, TRUE, NULL);
      qmi_client_wms_send_ack(client,
          input,
          10,
          user_data->cancellable,
          (GAsyncReadyCallback) send_ack_ready,
          user_data);
    }

    switch (msg_format)
    {
    case QMI_WMS_MESSAGE_FORMAT_CDMA:
      incoming_msg.msgInfo.format = SMS_NETTYPE_3GPP2;
      break;

    case QMI_WMS_MESSAGE_FORMAT_GSM_WCDMA_POINT_TO_POINT:
    case QMI_WMS_MESSAGE_FORMAT_GSM_WCDMA_BROADCAST:
    default:
      incoming_msg.msgInfo.format = SMS_NETTYPE_3GPP;
      break;
    }

    incoming_msg.msgInfo.msgLength = raw_data->len;
    memcpy(incoming_msg.msgInfo.tpduData, raw_data->data, raw_data->len);

    tcore_server_send_notification(s,
        co_sms, TNOTI_SMS_INCOM_MSG,
        sizeof(incoming_msg), &incoming_msg);
  }
}

static void set_event_report_ready(QmiClientWms *client, GAsyncResult *res,
    PluginData *user_data)
{
  QmiMessageWmsSetEventReportOutput *output;

  output = qmi_client_wms_set_event_report_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_wms_set_event_report_output_get_result(output, NULL))
  {
    user_data->wms_event_report_id = g_signal_connect(client,
        "event-report", G_CALLBACK(event_report), user_data);
  }

  qmi_message_wms_set_event_report_output_unref(output);
}

static void allocate_client_ready(QmiDevice *dev, GAsyncResult *res,
    PluginData *user_data)
{
  CoreObject *co_sms = tcore_plugin_ref_core_object(user_data->plugin,
      CORE_OBJECT_TYPE_SMS);
  QmiClient *client;

  client = qmi_device_allocate_client_finish(dev, res, NULL);
  user_data->wms_client = QMI_CLIENT_WMS(client);

  {
    QmiMessageWmsSetEventReportInput *input =
        qmi_message_wms_set_event_report_input_new();

    qmi_message_wms_set_event_report_input_set_new_mt_message_indicator(input,
        TRUE, NULL);
    qmi_client_wms_set_event_report(user_data->wms_client,
        input,
        10,
        user_data->cancellable,
        (GAsyncReadyCallback) set_event_report_ready,
        user_data);
    qmi_message_wms_set_event_report_input_unref(input);
  }

  tcore_sms_set_ready_status(co_sms, SMS_READY_STATUS_3GPP);

  user_data->sms_inited = TRUE;
}

static void release_client_ready(QmiDevice *dev, GAsyncResult *res, gpointer data)
{
  PluginData *user_data = (PluginData *) data;

  qmi_device_release_client_finish(dev, res, NULL);
  user_data->wms_client = NULL;
}


/* SMS Operations */
static struct tcore_sms_operations sms_ops = {
  .send_umts_msg = send_umts_msg,
  .read_msg = read_msg,
  .save_msg = NULL,
  .delete_msg = delete_msg,
  .get_storedMsgCnt = get_msg_count,
  .get_sca = NULL,
  .set_sca = NULL,
  .get_cb_config = NULL,
  .set_cb_config = NULL,
  .set_mem_status = NULL,
  .get_pref_brearer = NULL,
  .set_pref_brearer = NULL,
  .set_delivery_report = NULL,
  .set_msg_status = NULL,
  .get_sms_params = NULL,
  .set_sms_params = NULL,
  .get_paramcnt = NULL,
  .send_cdma_msg = NULL,
};


gboolean s_sms_init(TcorePlugin *p, PluginData *user_data)
{
  CoreObject *co_sms;

  co_sms = tcore_sms_new(p, "sms", &sms_ops, NULL);
  if (!co_sms)
  {
    err("Core object is NULL");
    return FALSE;
  }

  qmi_device_allocate_client(user_data->device,
      QMI_SERVICE_WMS,
      QMI_CID_NONE,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) allocate_client_ready,
      user_data);

  return TRUE;
}

void s_sms_exit(TcorePlugin *p)
{
  PluginData *user_data = tcore_plugin_ref_user_data(p);
  CoreObject *co_sms = tcore_plugin_ref_core_object(p, CORE_OBJECT_TYPE_SMS);

  if (user_data->wms_event_report_id)
  {
    g_signal_handler_disconnect(user_data->wms_client,
        user_data->wms_event_report_id);
  }

  qmi_device_release_client(user_data->device,
      QMI_CLIENT(user_data->wms_client),
      QMI_DEVICE_RELEASE_CLIENT_FLAGS_NONE,
      10,
      NULL,
      (GAsyncReadyCallback) release_client_ready,
      NULL);

  tcore_sms_free(co_sms);
}
