/**
 * tel-plugin-qmi
 *
 * Copyright (c) 2012 Samsung Electronics Co., Ltd. All rights reserved.
 * Copyright (c) 2021 Christof Meerwald. All rights reserved.
 *
 * Contact: Kyoungyoup Park <gynaru.park@samsung.com>
 *          Hayoon Ko       <hayoon.ko@samsung.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <glib.h>
#include <arpa/inet.h>

#include <tzplatform_config.h>
#include <tcore.h>
#include <core_object.h>
#include <plugin.h>
#include <server.h>
#include <co_ps.h>
#include <co_context.h>

#include "s_common.h"
#include "s_ps.h"


static CoreObject *curr_context;
static guchar curr_context_id;
static guint32 curr_packet_data_handle;


static void set_data_format_ready(QmiClientWda *client,
    GAsyncResult *res, gpointer data)
{
  QmiMessageWdaSetDataFormatOutput *output;

  output = qmi_client_wda_set_data_format_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_wda_set_data_format_output_get_result(output, NULL))
  {
  }

  qmi_message_wda_set_data_format_output_unref(output);
}

static void bind_data_port_ready(QmiClientWds *client,
    GAsyncResult *res, gpointer data)
{
  QmiMessageWdsBindDataPortOutput *output;

  output = qmi_client_wds_bind_data_port_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_wds_bind_data_port_output_get_result(output, NULL))
  {
  }

  qmi_message_wds_bind_data_port_output_unref(output);
}

static void set_ip_family_ready(QmiClientWds *client,
    GAsyncResult *res, gpointer data)
{
  QmiMessageWdsSetIpFamilyOutput *output;

  output = qmi_client_wds_set_ip_family_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_wds_set_ip_family_output_get_result(output, NULL))
  {
  }

  qmi_message_wds_set_ip_family_output_unref(output);

  {
    CoreObject *co_ps = (CoreObject *) data;
    TcorePlugin *p = tcore_object_ref_plugin(co_ps);
    Server *s = tcore_plugin_ref_server(p);

    struct tnoti_ps_call_status ps_call_status;

    ps_call_status.context_id = (guint)curr_context_id;
    ps_call_status.state = 0;
    ps_call_status.result = TCORE_RETURN_SUCCESS;

    /* Send PS CALL Status Notification */
    tcore_server_send_notification(s,
        co_ps, TNOTI_PS_CALL_STATUS,
        sizeof(ps_call_status), &ps_call_status);
  }
}

static TReturn define_context(CoreObject *co_ps,
    CoreObject *context, void *data)
{
  TcorePlugin *p = tcore_object_ref_plugin(co_ps);
  PluginData *user_data = tcore_plugin_ref_user_data(p);

  curr_context_id = tcore_context_get_id(context);
  curr_context = context;

  {
    QmiMessageWdaSetDataFormatInput *input =
        qmi_message_wda_set_data_format_input_new();

    qmi_message_wda_set_data_format_input_set_link_layer_protocol(input,
        QMI_WDA_LINK_LAYER_PROTOCOL_RAW_IP, NULL);
    qmi_message_wda_set_data_format_input_set_uplink_data_aggregation_protocol(input,
        QMI_WDA_DATA_AGGREGATION_PROTOCOL_DISABLED, NULL);
    qmi_message_wda_set_data_format_input_set_downlink_data_aggregation_protocol(input,
        QMI_WDA_DATA_AGGREGATION_PROTOCOL_DISABLED, NULL);

    qmi_client_wda_set_data_format(user_data->wda_client,
        input,
        10,
        user_data->cancellable,
        (GAsyncReadyCallback) set_data_format_ready,
        co_ps);

    qmi_message_wda_set_data_format_input_unref(input);
  }

  {
    QmiMessageWdsBindDataPortInput *input =
        qmi_message_wds_bind_data_port_input_new();

    qmi_message_wds_bind_data_port_input_set_data_port(input,
        QMI_SIO_PORT_A2_MUX_RMNET0, NULL);

    qmi_client_wds_bind_data_port(user_data->wds_client,
        input,
        10,
        user_data->cancellable,
        (GAsyncReadyCallback) bind_data_port_ready,
        co_ps);

    qmi_message_wds_bind_data_port_input_unref(input);
  }

  {
    QmiMessageWdsSetIpFamilyInput *input =
        qmi_message_wds_set_ip_family_input_new();

    qmi_message_wds_set_ip_family_input_set_preference(input,
        QMI_WDS_IP_FAMILY_IPV4, NULL);

    qmi_client_wds_set_ip_family(user_data->wds_client,
        input,
        10,
        user_data->cancellable,
        (GAsyncReadyCallback) set_ip_family_ready,
        co_ps);

    qmi_message_wds_set_ip_family_input_unref(input);
  }

  return TCORE_RETURN_SUCCESS;
}


static void start_network_ready(QmiClientWds *client,
    GAsyncResult *res, gpointer data)
{
  QmiMessageWdsStartNetworkOutput *output;

  output = qmi_client_wds_start_network_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_wds_start_network_output_get_result(output, NULL) &&
      qmi_message_wds_start_network_output_get_packet_data_handle(output,
          &curr_packet_data_handle, NULL))
  {
  }

  qmi_message_wds_start_network_output_unref(output);
}

static TReturn activate_context(CoreObject *co_ps,
    CoreObject *context, void *data)
{
  TcorePlugin *p = tcore_object_ref_plugin(co_ps);
  PluginData *user_data = tcore_plugin_ref_user_data(p);

  curr_context_id = tcore_context_get_id(context);
  curr_context = context;

  {
    QmiMessageWdsStartNetworkInput *input =
        qmi_message_wds_start_network_input_new();

    qmi_message_wds_start_network_input_set_apn(input,
        tcore_context_get_apn(context), NULL);
    qmi_message_wds_start_network_input_set_authentication_preference(input,
        QMI_WDS_AUTHENTICATION_NONE, NULL);
    qmi_message_wds_start_network_input_set_ip_family_preference(input,
        QMI_WDS_IP_FAMILY_IPV4, NULL);

    qmi_client_wds_start_network(user_data->wds_client,
        input,
        10,
        user_data->cancellable,
        (GAsyncReadyCallback) start_network_ready,
        data);

    qmi_message_wds_start_network_input_unref(input);
  }

  return TCORE_RETURN_SUCCESS;
}

static void stop_network_ready(QmiClientWds *client,
    GAsyncResult *res, gpointer data)
{
  QmiMessageWdsStopNetworkOutput *output;

  output = qmi_client_wds_stop_network_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_wds_stop_network_output_get_result(output, NULL))
  {
  }

  qmi_message_wds_stop_network_output_unref(output);
}

static TReturn deactivate_context(CoreObject *co_ps,
    CoreObject *context, void *data)
{
  TcorePlugin *p = tcore_object_ref_plugin(co_ps);
  PluginData *user_data = tcore_plugin_ref_user_data(p);

  QmiMessageWdsStopNetworkInput *input =
      qmi_message_wds_stop_network_input_new();

  qmi_message_wds_stop_network_input_set_packet_data_handle(input,
      curr_packet_data_handle, NULL);

  qmi_client_wds_stop_network(user_data->wds_client,
      input,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) stop_network_ready,
      user_data);

  qmi_message_wds_stop_network_input_unref(input);

  return TCORE_RETURN_SUCCESS;
}

static void get_current_settings_ready(QmiClientWds *client,
    GAsyncResult *res, gpointer data)
{
  QmiMessageWdsGetCurrentSettingsOutput *output;

  guint32 primary_ipv4_dns_address;
  guint32 secondary_ipv4_dns_address;
  guint32 ipv4_address;
  guint32 ipv4_gateway_address;
  guint32 ipv4_subnet_mask;
  guint32 mtu;

  output = qmi_client_wds_get_current_settings_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_wds_get_current_settings_output_get_result(output, NULL) &&
      qmi_message_wds_get_current_settings_output_get_primary_ipv4_dns_address(output,
          &primary_ipv4_dns_address, NULL) &&
      qmi_message_wds_get_current_settings_output_get_secondary_ipv4_dns_address(output,
          &secondary_ipv4_dns_address, NULL) &&
      qmi_message_wds_get_current_settings_output_get_ipv4_address(output,
          &ipv4_address, NULL) &&
      qmi_message_wds_get_current_settings_output_get_ipv4_gateway_address(output,
          &ipv4_gateway_address, NULL) &&
      qmi_message_wds_get_current_settings_output_get_ipv4_gateway_subnet_mask(output,
          &ipv4_subnet_mask, NULL) &&
      qmi_message_wds_get_current_settings_output_get_mtu(output,
          &mtu, NULL))
  {
    PluginData *user_data = (PluginData *) data;
    CoreObject *co_ps = tcore_plugin_ref_core_object(user_data->plugin, CORE_OBJECT_TYPE_PS);
    Server *s = tcore_plugin_ref_server(user_data->plugin);

    struct tnoti_ps_pdp_ipconfiguration ip_conf = { };
    struct tnoti_ps_call_status ps_call_status = { };

    ip_conf.context_id = curr_context_id;
    strcpy(ip_conf.devname, "wwan0");
    ip_conf.mtu = mtu;

    ipv4_address = htonl(ipv4_address);
    ipv4_gateway_address = htonl(ipv4_gateway_address);
    ipv4_subnet_mask = htonl(ipv4_subnet_mask);
    primary_ipv4_dns_address = htonl(primary_ipv4_dns_address);
    secondary_ipv4_dns_address = htonl(secondary_ipv4_dns_address);

    memcpy(ip_conf.ip_address, &ipv4_address, sizeof(ipv4_address));
    memcpy(ip_conf.primary_dns, &primary_ipv4_dns_address, sizeof(primary_ipv4_dns_address));
    memcpy(ip_conf.secondary_dns, &secondary_ipv4_dns_address, sizeof(secondary_ipv4_dns_address));

    {
      char mtustr[5];

      const char *iplink_argv[] = { "/sbin/ip",
        "link", "set", "dev", "wwan0", "up", "mtu", mtustr, NULL };

      sprintf(mtustr, "%4d", mtu);

      g_spawn_sync("/", (char **) iplink_argv, NULL,
          G_SPAWN_STDOUT_TO_DEV_NULL | G_SPAWN_STDERR_TO_DEV_NULL,
          NULL, NULL, NULL, NULL, NULL, NULL);
    }

    if (curr_context != NULL)
    {
      char ipaddr[INET_ADDRSTRLEN];

      sprintf(ipaddr, "%1d.%1d.%1d.%1d",
          ((unsigned char *) &ipv4_address)[0],
          ((unsigned char *) &ipv4_address)[1],
          ((unsigned char *) &ipv4_address)[2],
          ((unsigned char *) &ipv4_address)[3]);
      tcore_context_set_address(curr_context, ipaddr);

      sprintf(ipaddr, "%1d.%1d.%1d.%1d",
          ((unsigned char *) &primary_ipv4_dns_address)[0],
          ((unsigned char *) &primary_ipv4_dns_address)[1],
          ((unsigned char *) &primary_ipv4_dns_address)[2],
          ((unsigned char *) &primary_ipv4_dns_address)[3]);
      tcore_context_set_dns1(curr_context, ipaddr);

      sprintf(ipaddr, "%1d.%1d.%1d.%1d",
          ((unsigned char *) &secondary_ipv4_dns_address)[0],
          ((unsigned char *) &secondary_ipv4_dns_address)[1],
          ((unsigned char *) &secondary_ipv4_dns_address)[2],
          ((unsigned char *) &secondary_ipv4_dns_address)[3]);
      tcore_context_set_dns2(curr_context, ipaddr);
    }

    tcore_context_set_devinfo(curr_context, &ip_conf);

    tcore_server_send_notification(s,
        co_ps, TNOTI_PS_PDP_IPCONFIGURATION,
        sizeof(ip_conf), &ip_conf);

    ps_call_status.context_id = (guint)curr_context_id;
    ps_call_status.state = 1;
    ps_call_status.result = TCORE_RETURN_SUCCESS;

    /* Send PS CALL Status Notification */
    tcore_server_send_notification(s,
        co_ps, TNOTI_PS_CALL_STATUS,
        sizeof(ps_call_status), &ps_call_status);
  }

  qmi_message_wds_get_current_settings_output_unref(output);
}

static void event_report(QmiClientWds *client,
    QmiIndicationWdsEventReportOutput *output, PluginData *user_data)
{
  CoreObject *co_ps = tcore_plugin_ref_core_object(user_data->plugin,
      CORE_OBJECT_TYPE_PS);
  Server *s = tcore_plugin_ref_server(user_data->plugin);

  QmiWdsDataCallStatus call_status;
  QmiWdsDataBearerTechnology data_bearer_technology;

  if (qmi_indication_wds_event_report_output_get_data_call_status(output,
          &call_status, NULL))
  {
    switch (call_status)
    {
    case QMI_WDS_DATA_CALL_STATUS_ACTIVATED:
      {
        QmiMessageWdsGetCurrentSettingsInput *input =
            qmi_message_wds_get_current_settings_input_new();

        qmi_message_wds_get_current_settings_input_set_requested_settings(input,
            QMI_WDS_GET_CURRENT_SETTINGS_REQUESTED_SETTINGS_DNS_ADDRESS |
            QMI_WDS_GET_CURRENT_SETTINGS_REQUESTED_SETTINGS_IP_ADDRESS |
            QMI_WDS_GET_CURRENT_SETTINGS_REQUESTED_SETTINGS_GATEWAY_INFO |
            QMI_WDS_GET_CURRENT_SETTINGS_REQUESTED_SETTINGS_MTU |
            QMI_WDS_GET_CURRENT_SETTINGS_REQUESTED_SETTINGS_IP_FAMILY,
            NULL);

        qmi_client_wds_get_current_settings(user_data->wds_client,
            input,
            10,
            user_data->cancellable,
            (GAsyncReadyCallback) get_current_settings_ready,
            user_data);

        qmi_message_wds_get_current_settings_input_unref(input);
      }
      break;

    case QMI_WDS_DATA_CALL_STATUS_UNKNOWN:
    case QMI_WDS_DATA_CALL_STATUS_TERMINATED:
    default:
      {
        struct tnoti_ps_call_status ps_call_status = { };

        ps_call_status.context_id = (guint)curr_context_id;
        ps_call_status.state = 3;
        ps_call_status.result = TCORE_RETURN_SUCCESS;

        {
          const char *iplink_argv[] = { "/sbin/ip",
            "link", "set", "wwan0", "down", NULL };

          g_spawn_sync("/", (char **) iplink_argv, NULL,
              G_SPAWN_STDOUT_TO_DEV_NULL | G_SPAWN_STDERR_TO_DEV_NULL,
              NULL, NULL, NULL, NULL, NULL, NULL);
        }

        tcore_server_send_notification(s,
            co_ps, TNOTI_PS_CALL_STATUS,
            sizeof(ps_call_status), &ps_call_status);
      }
      break;
    }
  }
  if (qmi_indication_wds_event_report_output_get_data_bearer_technology(output,
          &data_bearer_technology, NULL))
  {
    struct tnoti_ps_protocol_status protocol_status = { };

    switch (data_bearer_technology)
    {
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_HSDPA:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_TDSCDMA_HSDPA:
      protocol_status.status = TELEPHONY_HSDPA_ON;
      break;

    case QMI_WDS_DATA_BEARER_TECHNOLOGY_HSUPA:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_HSDPA_HSUPDA:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_TDSCDMA_HSUPA:
      protocol_status.status = TELEPHONY_HSUPA_ON;
      break;

    case QMI_WDS_DATA_BEARER_TECHNOLOGY_EHRPD:
      protocol_status.status = TELEPHONY_HSPA_ON;
      break;

    case QMI_WDS_DATA_BEARER_TECHNOLOGY_HSDPAPLUS:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_HSDPAPLUS_HSUPA:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_DCHSDPAPLUS:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_DCHSDPAPLUS_HSUPA:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_HSDPAPLUS_PLUS_64QAM:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_HSDPAPLUS_PLUS_64QAM_HSUPA:
      protocol_status.status = TELEPHONY_HSPAP_ON;
      break;

    case QMI_WDS_DATA_BEARER_TECHNOLOGY_UNKNOWN:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_TDSCDMA:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_CDMA20001X:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_1xEVDO:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_GSM:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_UMTS:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_1xEVDO_REVA:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_EDGE:
    case QMI_WDS_DATA_BEARER_TECHNOLOGY_LTE:
    default:
      protocol_status.status = TELEPHONY_HSDPA_OFF;
      break;
    }

    tcore_server_send_notification(s,
        co_ps, TNOTI_PS_PROTOCOL_STATUS,
        sizeof(protocol_status), &protocol_status);
  }
}

static void set_event_report_ready(QmiClientWds *client,
    GAsyncResult *res, PluginData *user_data)
{
  CoreObject *co_ps = tcore_plugin_ref_core_object(user_data->plugin,
      CORE_OBJECT_TYPE_PS);
  QmiMessageWdsSetEventReportOutput *output;

  output = qmi_client_wds_set_event_report_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_wds_set_event_report_output_get_result(output, NULL))
  {
    user_data->wds_event_report_id = g_signal_connect(client,
        "event-report", G_CALLBACK(event_report), user_data);
  }

  tcore_ps_set_online(co_ps, TRUE);

  qmi_message_wds_set_event_report_output_unref(output);
}

static void allocate_wda_client_ready(QmiDevice *dev,
    GAsyncResult *res, PluginData *user_data)
{
  GError *error = NULL;
  QmiClient *client;

  client = qmi_device_allocate_client_finish(dev, res, &error);

  user_data->wda_client = QMI_CLIENT_WDA(client);
  user_data->ps_inited = (user_data->wda_client != NULL) &&
      (user_data->wds_client != NULL);
}

static void allocate_wds_client_ready(QmiDevice *dev,
    GAsyncResult *res, PluginData *user_data)
{
  GError *error = NULL;
  QmiClient *client;

  client = qmi_device_allocate_client_finish(dev, res, &error);
  user_data->wds_client = QMI_CLIENT_WDS(client);

  {
    QmiMessageWdsSetEventReportInput *input =
        qmi_message_wds_set_event_report_input_new();

    qmi_message_wds_set_event_report_input_set_data_call_status(input,
        TRUE, NULL);
    qmi_message_wds_set_event_report_input_set_data_bearer_technology(input,
        TRUE, NULL);
    qmi_client_wds_set_event_report(user_data->wds_client,
        input,
        10,
        user_data->cancellable,
        (GAsyncReadyCallback) set_event_report_ready,
        user_data);
    qmi_message_wds_set_event_report_input_unref(input);
  }

  user_data->ps_inited = (user_data->wda_client != NULL) && (user_data->wds_client != NULL);
}

static void release_wda_client_ready(QmiDevice *dev,
    GAsyncResult *res, PluginData *user_data)
{
  qmi_device_release_client_finish(dev, res, NULL);
  user_data->wda_client = NULL;
}

static void release_wds_client_ready(QmiDevice *dev,
    GAsyncResult *res, PluginData *user_data)
{
  qmi_device_release_client_finish(dev, res, NULL);
  user_data->wds_client = NULL;
}


/* PS Operations */
static struct tcore_ps_operations ps_ops = {
  .define_context = define_context,
  .activate_context = activate_context,
  .deactivate_context = deactivate_context
};

gboolean s_ps_init(TcorePlugin *p, PluginData *user_data)
{
  CoreObject *o;

  o = tcore_ps_new(p, "umts_ps", &ps_ops, NULL);
  if (!o)
  {
    return FALSE;
  }

  qmi_device_allocate_client(user_data->device,
      QMI_SERVICE_WDA,
      QMI_CID_NONE,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) allocate_wda_client_ready,
      user_data);

  qmi_device_allocate_client(user_data->device,
      QMI_SERVICE_WDS,
      QMI_CID_NONE,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) allocate_wds_client_ready,
      user_data);

  return TRUE;
}

void s_ps_exit(TcorePlugin *p)
{
  PluginData *user_data = tcore_plugin_ref_user_data(p);
  CoreObject *co_ps = tcore_plugin_ref_core_object(p, CORE_OBJECT_TYPE_PS);

  if (user_data->wds_event_report_id)
  {
    g_signal_handler_disconnect(user_data->wds_client,
        user_data->wds_event_report_id);
  }

  qmi_device_release_client(user_data->device,
      QMI_CLIENT(user_data->wda_client),
      QMI_DEVICE_RELEASE_CLIENT_FLAGS_NONE,
      10,
      NULL,
      (GAsyncReadyCallback) release_wda_client_ready,
      NULL);

  qmi_device_release_client(user_data->device,
      QMI_CLIENT(user_data->wds_client),
      QMI_DEVICE_RELEASE_CLIENT_FLAGS_NONE,
      10,
      NULL,
      (GAsyncReadyCallback) release_wds_client_ready,
      NULL);


  tcore_ps_free(co_ps);
}
