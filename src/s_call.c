/*
 * tel-plugin-qmi
 *
 * Copyright (c) 2012 Samsung Electronics Co., Ltd. All rights reserved.
 * Copyright (c) 2021 Christof Meerwald. All rights reserved.
 *
 * Contact: Hayoon Ko <hayoon.ko@samsung.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>

#include <tzplatform_config.h>
#include <tcore.h>
#include <user_request.h>
#include <core_object.h>
#include <plugin.h>
#include <server.h>
#include <co_call.h>

#include "s_common.h"
#include "s_call.h"


static void call_dial_ready(QmiClientVoice *client, GAsyncResult *res,
    UserRequest *ur)
{
  GError *error = NULL;
  QmiMessageVoiceDialCallOutput *output;
  struct tresp_call_dial resp = { };

  guint8 call_id;

  resp.err = CALL_ERROR_UNKNOWN;

  output = qmi_client_voice_dial_call_finish(client, res, &error);
  if (!output)
  {
    g_error("error: couldn't dial: %s\n", error->message);
  }
  else if (qmi_message_voice_dial_call_output_get_result(output, NULL) &&
      qmi_message_voice_dial_call_output_get_call_id(output, &call_id, NULL))
  {
    resp.err = CALL_ERROR_NONE;
  }

  tcore_user_request_send_response(ur, TRESP_CALL_DIAL,
      sizeof(resp), &resp);

  qmi_message_voice_dial_call_output_unref(output);
}

static TReturn call_dial(CoreObject *co_call, UserRequest *ur)
{
  PluginData *user_data;
  QmiMessageVoiceDialCallInput *input;
  const struct treq_call_dial *req;

  user_data = tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_call));
  req = tcore_user_request_ref_data(ur, NULL);

  input = qmi_message_voice_dial_call_input_new();

  qmi_message_voice_dial_call_input_set_calling_number(input,
      req->number, NULL);

  qmi_client_voice_dial_call(user_data->voice_client,
      input,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) call_dial_ready,
      ur);

  qmi_message_voice_dial_call_input_unref(input);

  return TCORE_RETURN_SUCCESS;
}

static void call_end_ready(QmiClientVoice *client, GAsyncResult *res,
    UserRequest *ur)
{
  GError *error = NULL;
  QmiMessageVoiceEndCallOutput *output;
  const struct treq_call_end *req;
  struct tresp_call_end resp = { };

  guint8 call_id;

  req = tcore_user_request_ref_data(ur, NULL);
  resp.err = CALL_ERROR_UNKNOWN;
  resp.handle = req->handle;

  output = qmi_client_voice_end_call_finish(client, res, &error);
  if (!output)
  {
    g_error("error: couldn't end: %s\n", error->message);
  }
  else if (qmi_message_voice_end_call_output_get_result(output, NULL) &&
      qmi_message_voice_end_call_output_get_call_id(output, &call_id, NULL))
  {
    resp.err = CALL_ERROR_NONE;
  }

  tcore_user_request_send_response(ur, TRESP_CALL_END,
      sizeof(resp), &resp);

  qmi_message_voice_end_call_output_unref(output);
}

static TReturn call_end(CoreObject *co_call, UserRequest *ur)
{
  PluginData *user_data;
  const struct treq_call_end *req;

  CallObject *call_obj;

  user_data = tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_call));
  req = tcore_user_request_ref_data(ur, NULL);

  call_obj = tcore_call_object_find_by_handle(co_call, req->handle);
  if (call_obj != NULL)
  {
    QmiMessageVoiceEndCallInput *input = qmi_message_voice_end_call_input_new();

    qmi_message_voice_end_call_input_set_call_id(input,
        tcore_call_object_get_id(call_obj), NULL);

    qmi_client_voice_end_call(user_data->voice_client,
        input,
        10,
        user_data->cancellable,
        (GAsyncReadyCallback) call_end_ready,
        ur);

    qmi_message_voice_end_call_input_unref(input);
  }
  else
  {
    struct tresp_call_end resp = { };

    resp.err = CALL_ERROR_UNKNOWN;
    resp.handle = req->handle;

    tcore_user_request_send_response(ur, TRESP_CALL_END,
        sizeof(resp), &resp);
  }

  return TCORE_RETURN_SUCCESS;
}

static void call_answer_ready(QmiClientVoice *client, GAsyncResult *res,
    UserRequest *ur)
{
  GError *error = NULL;
  QmiMessageVoiceAnswerCallOutput *output;
  const struct treq_call_answer *req;
  struct tresp_call_answer resp = { };

  guint8 call_id;

  req = tcore_user_request_ref_data(ur, NULL);
  resp.err = CALL_ERROR_UNKNOWN;
  resp.handle = req->handle;

  output = qmi_client_voice_answer_call_finish(client, res, &error);
  if (!output)
  {
    g_error("error: couldn't answer: %s\n", error->message);
  }
  else if (qmi_message_voice_answer_call_output_get_result(output, NULL) &&
      qmi_message_voice_answer_call_output_get_call_id(output, &call_id, NULL))
  {
    resp.err = CALL_ERROR_NONE;
  }

  tcore_user_request_send_response(ur, TRESP_CALL_ANSWER,
      sizeof(resp), &resp);

  qmi_message_voice_answer_call_output_unref(output);
}

static void call_reject_ready(QmiClientVoice *client, GAsyncResult *res,
    UserRequest *ur)
{
  GError *error = NULL;
  QmiMessageVoiceEndCallOutput *output;
  const struct treq_call_answer *req;
  struct tresp_call_answer resp = { };

  guint8 call_id;

  req = tcore_user_request_ref_data(ur, NULL);
  resp.err = CALL_ERROR_UNKNOWN;
  resp.handle = req->handle;

  output = qmi_client_voice_end_call_finish(client, res, &error);
  if (!output)
  {
    g_error("error: couldn't answer: %s\n", error->message);
  }
  else if (qmi_message_voice_end_call_output_get_result(output, NULL) &&
      qmi_message_voice_end_call_output_get_call_id(output, &call_id, NULL))
  {
    resp.err = CALL_ERROR_NONE;
  }

  tcore_user_request_send_response(ur, TRESP_CALL_ANSWER,
      sizeof(resp), &resp);

  qmi_message_voice_end_call_output_unref(output);
}

static TReturn call_answer(CoreObject *co_call, UserRequest *ur)
{
  PluginData *user_data;
  const struct treq_call_answer *req;

  CallObject *call_obj;

  user_data = tcore_plugin_ref_user_data(tcore_object_ref_plugin(co_call));
  req = tcore_user_request_ref_data(ur, NULL);

  call_obj = tcore_call_object_find_by_handle(co_call, req->handle);
  if (call_obj != NULL)
  {
    switch (req->type)
    {
    case CALL_ANSWER_TYPE_ACCEPT:
    case CALL_ANSWER_TYPE_REPLACE:
    case CALL_ANSWER_TYPE_HOLD_ACCEPT:
      {
        QmiMessageVoiceAnswerCallInput *input =
            qmi_message_voice_answer_call_input_new();

        qmi_message_voice_answer_call_input_set_call_id(input,
            tcore_call_object_get_id(call_obj), NULL);

        qmi_client_voice_answer_call(user_data->voice_client,
            input,
            10,
            user_data->cancellable,
            (GAsyncReadyCallback) call_answer_ready,
            ur);

        qmi_message_voice_answer_call_input_unref(input);
      }
      break;

    case CALL_ANSWER_TYPE_REJECT:
    default:
      {
        QmiMessageVoiceEndCallInput *input =
            qmi_message_voice_end_call_input_new();

        qmi_message_voice_end_call_input_set_call_id(input,
            tcore_call_object_get_id(call_obj), NULL);

        qmi_client_voice_end_call(user_data->voice_client,
            input,
            10,
            user_data->cancellable,
            (GAsyncReadyCallback) call_reject_ready,
            ur);

        qmi_message_voice_end_call_input_unref(input);
      }
      break;
    }
  }
  else
  {
    struct tresp_call_answer resp = { };

    resp.err = CALL_ERROR_UNKNOWN;
    resp.handle = req->handle;

    tcore_user_request_send_response(ur, TRESP_CALL_ANSWER,
        sizeof(resp), &resp);
  }

  return TCORE_RETURN_SUCCESS;
}


static void call_status(QmiClientWms *client,
    QmiIndicationVoiceAllCallStatusOutput *output, PluginData *user_data)
{
  CoreObject *co_call = tcore_plugin_ref_core_object(user_data->plugin,
      CORE_OBJECT_TYPE_CALL);
  Server *s = tcore_plugin_ref_server(user_data->plugin);

  GArray *remote_party_number = NULL;
  GArray *call_information = NULL;

  qmi_indication_voice_all_call_status_output_get_remote_party_number(output,
      &remote_party_number, NULL);

  if (qmi_indication_voice_all_call_status_output_get_call_information(output,
          &call_information, NULL) &&
      (call_information != NULL))
  {
    guint i;

    for (i = 0; i != call_information->len; ++i)
    {
      CallObject *call_obj = NULL;
      QmiIndicationVoiceAllCallStatusOutputCallInformationCall *call_elem =
          &g_array_index(
            call_information,
            QmiIndicationVoiceAllCallStatusOutputCallInformationCall, i);
      QmiIndicationVoiceAllCallStatusOutputRemotePartyNumberCall *number_elem = NULL;

      if (remote_party_number != NULL)
      {
        guint j;

        for (j = 0; j != remote_party_number->len; ++j)
        {
          number_elem = &g_array_index(
            remote_party_number,
            QmiIndicationVoiceAllCallStatusOutputRemotePartyNumberCall, j);
          if (number_elem->id == call_elem->id)
          {
            break;
          }

          number_elem = NULL;
        }
      }

      call_obj = tcore_call_object_find_by_id(co_call, call_elem->id);

      if (call_obj == NULL) {
        switch (call_elem->state)
        {
        case QMI_VOICE_CALL_STATE_SETUP:
        case QMI_VOICE_CALL_STATE_INCOMING:
        case QMI_VOICE_CALL_STATE_CC_IN_PROGRESS:
        case QMI_VOICE_CALL_STATE_ORIGINATION:
          call_obj = tcore_call_object_new(co_call);

          tcore_call_object_set_id(call_obj, call_elem->id);
          tcore_call_object_set_type(call_obj, CALL_TYPE_VOICE);
          tcore_call_object_set_multiparty_state(call_obj, FALSE);

          if (number_elem != NULL)
          {
            enum tcore_call_cli_mode mode;
            enum tcore_call_no_cli_cause no_cli_cause = CALL_NO_CLI_CAUSE_NONE;

            switch (number_elem->presentation_indicator)
            {
            case QMI_VOICE_PRESENTATION_ALLOWED:
            case QMI_VOICE_PRESENTATION_PAYPHONE:
              mode = CALL_CLI_MODE_PRESENT;
              break;

            case QMI_VOICE_PRESENTATION_RESTRICTED:
              mode = CALL_CLI_MODE_RESTRICT;
              break;

            case QMI_VOICE_PRESENTATION_UNAVAILABLE:
              mode = CALL_CLI_MODE_UNAVAILABLE;
              no_cli_cause = CALL_NO_CLI_CAUSE_UNAVAILABLE;
              break;

            default:
              mode = CALL_CLI_MODE_DEFAULT;
              break;
            }

            tcore_call_object_set_cli_info(call_obj,
                mode, no_cli_cause,
                number_elem->type, strlen(number_elem->type));
          }
          break;

        default:
          break;
        }
      }

      if (call_obj == NULL)
      {
        continue;
      }

      switch (call_elem->state)
      {
      case QMI_VOICE_CALL_STATE_INCOMING:
        {
          struct tnoti_call_status_incoming incoming_call = { };

          incoming_call.type = CALL_TYPE_VOICE;
          incoming_call.handle = tcore_call_object_get_handle(call_obj);
          incoming_call.active_line = 0;
          if (number_elem != NULL)
          {
            strcpy(incoming_call.number_plus, number_elem->type);

            incoming_call.cli.no_cli_cause = CALL_NO_CLI_CAUSE_NONE;
            incoming_call.cli.mode = tcore_call_object_get_cli_mode(call_obj);
            incoming_call.cli.no_cli_cause = tcore_call_object_get_no_cli_cause(call_obj);

            tcore_call_object_get_number(call_obj, incoming_call.cli.number);
          }
          else
          {
            incoming_call.cli.mode = CALL_CLI_MODE_UNAVAILABLE;
            incoming_call.cli.no_cli_cause = CALL_NO_CLI_CAUSE_OTHERS;
          }

          tcore_call_object_set_direction(call_obj,
              TCORE_CALL_DIRECTION_INCOMING);
          tcore_call_object_set_status(call_obj, CALL_STATUS_INCOMING);

          tcore_server_send_notification(s,
              co_call, TNOTI_CALL_STATUS_INCOMING,
              sizeof(incoming_call), &incoming_call);
        }
        break;

      case QMI_VOICE_CALL_STATE_DISCONNECTING:
        {
          struct tnoti_call_status_idle idle_call = { };

          tcore_call_object_set_status(call_obj, CALL_STATUS_IDLE);

          idle_call.type = CALL_TYPE_VOICE;
          idle_call.handle = tcore_call_object_get_handle(call_obj);
          idle_call.cause = CALL_END_CAUSE_NONE;

          tcore_server_send_notification(s,
              co_call, TNOTI_CALL_STATUS_IDLE,
              sizeof(idle_call), &idle_call);
        }
        break;

      case QMI_VOICE_CALL_STATE_END:
        {
          tcore_call_object_free(co_call, call_obj);
        }
        break;

      case QMI_VOICE_CALL_STATE_ALERTING:
        {
          struct tnoti_call_status_alert alert_call = { };

          tcore_call_object_set_status(call_obj, CALL_STATUS_ALERT);

          alert_call.type = CALL_TYPE_VOICE;
          alert_call.handle = tcore_call_object_get_handle(call_obj);

          tcore_server_send_notification(s,
              co_call, TNOTI_CALL_STATUS_ALERT,
              sizeof(alert_call), &alert_call);
        }
        break;

      case QMI_VOICE_CALL_STATE_CC_IN_PROGRESS:
        {
          tcore_call_object_set_direction(call_obj,
              TCORE_CALL_DIRECTION_OUTGOING);
          tcore_call_object_set_status(call_obj, CALL_STATUS_DIALING);
        }
        break;

      case QMI_VOICE_CALL_STATE_ORIGINATION:
        {
          struct tnoti_call_status_dialing dialing_call = { };

          tcore_call_object_set_direction(call_obj,
              TCORE_CALL_DIRECTION_OUTGOING);
          tcore_call_object_set_status(call_obj, CALL_STATUS_DIALING);

          dialing_call.type = CALL_TYPE_VOICE;
          dialing_call.handle = tcore_call_object_get_handle(call_obj);

          tcore_server_send_notification(s,
              co_call, TNOTI_CALL_STATUS_DIALING,
              sizeof(dialing_call), &dialing_call);
        }
        break;

      case QMI_VOICE_CALL_STATE_CONVERSATION:
        {
          struct tnoti_call_status_active active_call = { };

          tcore_call_object_set_status(call_obj, CALL_STATUS_ACTIVE);

          active_call.type = CALL_TYPE_VOICE;
          active_call.handle = tcore_call_object_get_handle(call_obj);

          tcore_server_send_notification(s,
              co_call, TNOTI_CALL_STATUS_ACTIVE,
              sizeof(active_call), &active_call);
        }
        break;

      case QMI_VOICE_CALL_STATE_HOLD:
        {
          struct tnoti_call_status_held held_call = { };

          tcore_call_object_set_status(call_obj, CALL_STATUS_HELD);

          held_call.type = CALL_TYPE_VOICE;
          held_call.handle = tcore_call_object_get_handle(call_obj);

          tcore_server_send_notification(s,
              co_call, TNOTI_CALL_STATUS_HELD,
              sizeof(held_call), &held_call);
        }
        break;

      case QMI_VOICE_CALL_STATE_WAITING:
        {
          struct tnoti_call_status_waiting waiting_call = { };

          tcore_call_object_set_status(call_obj, CALL_STATUS_WAITING);

          waiting_call.type = CALL_TYPE_VOICE;
          waiting_call.handle = tcore_call_object_get_handle(call_obj);

          tcore_server_send_notification(s,
              co_call, TNOTI_CALL_STATUS_WAITING,
              sizeof(waiting_call), &waiting_call);
        }
        break;

      default:
        break;
      }
    }
  }
}

static void voice_indication_register_ready(QmiClientVoice *client,
    GAsyncResult *res, PluginData *user_data)
{
  QmiMessageVoiceIndicationRegisterOutput *output;

  output = qmi_client_voice_indication_register_finish(client, res, NULL);
  if ((output != NULL) &&
      qmi_message_voice_indication_register_output_get_result(output, NULL))
  {
    user_data->voice_call_status_id = g_signal_connect(client,
        "all-call-status", G_CALLBACK(call_status), user_data);
  }

  qmi_message_voice_indication_register_output_unref(output);
}

static void allocate_client_ready(QmiDevice *dev, GAsyncResult *res, gpointer data)
{
  PluginData *user_data = (PluginData *) data;
  GError *error = NULL;
  QmiClient *client;

  client = qmi_device_allocate_client_finish(dev, res, &error);

  user_data->voice_client = QMI_CLIENT_VOICE(client);

  {
    QmiMessageVoiceIndicationRegisterInput *input =
        qmi_message_voice_indication_register_input_new();

    qmi_message_voice_indication_register_input_set_call_notification_events(input,
        TRUE, NULL);
    qmi_client_voice_indication_register(user_data->voice_client,
        input,
        10,
        user_data->cancellable,
        (GAsyncReadyCallback) voice_indication_register_ready,
        data);
    qmi_message_voice_indication_register_input_unref(input);
  }

  user_data->call_inited = TRUE;
}

static void release_client_ready(QmiDevice *dev, GAsyncResult *res, gpointer data)
{
  PluginData *user_data = (PluginData *) data;

  qmi_device_release_client_finish(dev, res, NULL);
  user_data->voice_client = NULL;
}


/* CALL Operations */
static struct tcore_call_operations call_ops = {
  .dial = call_dial,
  .answer = call_answer,
  .end = call_end,
  .hold = NULL,
  .active = NULL,
  .swap = NULL,
  .join = NULL,
  .split = NULL,
  .deflect = NULL,
  .transfer = NULL,
  .start_cont_dtmf = NULL,
  .stop_cont_dtmf = NULL,
  .send_burst_dtmf = NULL,
  .set_sound_path = NULL,
  .set_sound_volume_level = NULL,
  .get_sound_volume_level = NULL,
  .set_sound_mute_status = NULL,
  .get_sound_mute_status = NULL,
  .set_preferred_voice_subscription = NULL,
  .get_preferred_voice_subscription = NULL,
};


gboolean s_call_init(TcorePlugin *p, PluginData *user_data)
{
  CoreObject *co_call;

  co_call = tcore_call_new(p, "call", &call_ops, NULL);
  if (!co_call)
  {
    err("Core object is NULL");
    return FALSE;
  }

  qmi_device_allocate_client(user_data->device,
      QMI_SERVICE_VOICE,
      QMI_CID_NONE,
      10,
      user_data->cancellable,
      (GAsyncReadyCallback) allocate_client_ready,
      user_data);

  return TRUE;
}

void s_call_exit(TcorePlugin *p)
{
  PluginData *user_data = tcore_plugin_ref_user_data(p);
  CoreObject *co_call = tcore_plugin_ref_core_object(p, CORE_OBJECT_TYPE_CALL);

  if (user_data->voice_call_status_id)
  {
    g_signal_handler_disconnect(user_data->voice_client,
        user_data->voice_call_status_id);
  }

  qmi_device_release_client(user_data->device,
      QMI_CLIENT(user_data->voice_client),
      QMI_DEVICE_RELEASE_CLIENT_FLAGS_NONE,
      10,
      NULL,
      (GAsyncReadyCallback) release_client_ready,
      NULL);

  tcore_call_free(co_call);
}
