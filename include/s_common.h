/*
 * tel-plugin-qmi
 *
 * Copyright (c) 2021 Christof Meerwald. All rights reserved.
 *
 * Contact: Christof Meerwald <cmeerw@cmeerw.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __S_COMMON_H__
#define __S_COMMON_H__

#include <libqmi-glib.h>

typedef struct {
  TcorePlugin *plugin;

  GCancellable *cancellable;
  GFile *file;

  QmiDevice *device;

  gboolean device_opened : 1;
  gboolean call_inited : 1;
  gboolean modem_inited : 1;
  gboolean network_inited : 1;
  gboolean ps_inited : 1;
  gboolean sim_inited : 1;
  gboolean sms_inited : 1;

  QmiClientDms *dms_client;
  gulong dms_event_report_id;

  QmiClientNas *nas_client;
  gulong nas_event_report_id;
  gulong nas_serving_system_id;

  QmiClientUim *uim_client;
  gulong uim_slot_status_id;

  QmiClientVoice *voice_client;
  gulong voice_call_status_id;

  QmiClientWda *wda_client;
  QmiClientWds *wds_client;
  gulong wds_event_report_id;

  QmiClientWms *wms_client;
  gulong wms_event_report_id;
} PluginData;

#endif
